# Contributing to tor-file-parser

If you have questions or comments, please feel free to email me (details in
[`Cargo.toml`](https://gitlab.com/DrPeterVanNostrand/tor-file-parser/blob/master/Cargo.toml)).

For feature requests, suggestions, and bug reports, please open an issue.

Patches are welcomed in the form pull requests.
