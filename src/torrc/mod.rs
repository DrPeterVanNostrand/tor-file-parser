use std::fmt::{self, Display, Formatter};
use std::io::Read;
use std::path::Path;

use iter::ItemIter;
use keywords::TORRC_KEYWORDS;
use log::log_unparsed_item;
use parsers::torrc::*;

const DEFAULT_TORRC_PATHS: [&'static str; 5] = [
    "/usr/local/etc/tor/torrc",
    "/etc/tor/torrc",
    "/etc/torrc",
    "~/.tor/torrc",
    "~/.torrc",
];

#[derive(Debug, Default, PartialEq)]
pub struct Torrc {
    pub control_port: Option<u16>
}

impl Torrc {
    pub fn empty() -> Self { Torrc::default() }
    pub fn is_empty(&self) -> bool { *self == Torrc::empty() }

    pub fn new<R: Read>(readable: R) -> Self {
        let mut torrc = Torrc::empty();
        let items = ItemIter::from_reader(readable, &TORRC_KEYWORDS);

        for item in items {
            if !TORRC_KEYWORDS.contains(&item.keyword()) { continue; }
            match item.keyword() {
                "ControlPort" =>
                    torrc.control_port = parse_control_port(&item).ok(),
                _ => log_unparsed_item(&item)
            }
        }

        torrc
    }

    pub fn find_file() -> Option<String> {
        let home_dir = env!("HOME");
        for path in DEFAULT_TORRC_PATHS.iter() {
            let expanded = path.replace("~/", home_dir);
            if Path::new(&expanded).is_file() {
                return Some(expanded);
            }
        }
        None
    }
}

impl Display for Torrc {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{:#?}", self)
    }
}

#[test]
fn test_torrc() {
    let bytes =
        "## The port on which Tor will listen for local connections from Tor\n\
        ## controller applications, as documented in control-spec.txt.\n\
        ControlPort 9051\n\
        ## If you enable the controlport, be sure to enable one of these\n\
        ## authentication methods, to prevent attackers from accessing it.".as_bytes();

    let torrc = Torrc::new(bytes);
    assert!(!torrc.is_empty());
    assert_eq!(torrc.control_port, Some(9051));
}
