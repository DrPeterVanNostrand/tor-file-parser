use std::fmt::Display;

use descriptor::DescItem;

pub fn log_item_missing_data(item: &DescItem) {
    eprintln!("Item Missing Data: `{}`", item);
}

pub fn log_unrecognized_pluggable_transport(item: &DescItem) {
    eprintln!("Unrecognized Pluggable Transport: `{}`", item);
}

pub fn log_unparsed_item(item: &DescItem) {
    eprintln!("Unparsed Item: `{}`", item);
}

pub fn log_type_conversion_error<T>(item: &DescItem, invalid: T, expected: &str)
    where T: Display
{
    eprintln!(
        "Type Conversion Error: could not convert `{}` to {}.\n---> {}.",
        invalid,
        expected,
        item
    );
}
