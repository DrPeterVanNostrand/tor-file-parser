pub const ALL_KEYWORDS: [&'static str; 109] = [
    // Keywords that are found in multiple document types.
    "@type",
    "identity-ed25519",
    "master-key-ed25519",
    "published",
    "fingerprint",
    "router-sig-ed25519",
    "router-signature",
    "router-digest-sha256",
    "router-digest",
    "family",

    // Keywords found in server-descriptor documents.
    "@purpose",
    "router",
    "platform",
    "proto",
    "uptime",
    "bandwidth",
    "extra-info-digest",
    "onion-key",
    "signing-key",
    "onion-key-crosscert",
    "ntor-onion-key-crosscert",
    "hidden-service-dir",
    "contact",
    "ntor-onion-key",
    "reject",
    "accept",
    "ipv6-policy",
    "tunnelled-dir-server",
    "or-address",
    "hibernating",
    "bridge-distribution-request",
    "caches-extra-info",
    "protocols",

    // Keywords found in extra-info documents.
    "extra-info",
    "write-history",
    "read-history",
    "dirreq-write-history",
    "dirreq-read-history",
    "geoip-db-digest",
    "geoip6-db-digest",
    "dirreq-stats-end",
    "dirreq-v3-ips",
    "dirreq-v3-reqs",
    "dirreq-v3-resp",
    "dirreq-v3-direct-dl",
    "dirreq-v3-tunneled-dl",
    "hidserv-stats-end",
    "hidserv-rend-relayed-cells",
    "hidserv-dir-onions-seen",
    "padding-counts",
    "transport",
    "bridge-stats-end",
    "bridge-ips",
    "bridge-ip-versions",
    "bridge-ip-transports",
    "entry-stats-end",
    "entry-ips",
    "cell-stats-end",
    "cell-processed-cells",
    "cell-queued-cells",
    "cell-time-in-queue",
    "cell-circuits-per-decile",
    "exit-stats-end",
    "exit-kibibytes-written",
    "exit-kibibytes-read",
    "exit-streams-opened",
    "conn-bi-direct",

    // Keywords found in network-status documents.
    "network-status-version",
    "flag-thresholds",
    "r",
    "a",
    "s",
    "w",
    "p",
    "v",
    "m",
    "pr",
    "id",

    // Keywords found in consenus and vote headers.
    "network-status-version",
    "vote-status",
    "consensus-method" ,
    "valid-after",
    "fresh-until",
    "valid-until",
    "voting-delay",
    "client-versions",
    "server-versions",
    "known-flags",
    "recommended-client-protocols",
    "recommended-relay-protocols",
    "required-client-protocols",
    "required-relay-protocols",
    "params",
    "shared-rand-previous-value",
    "shared-rand-current-value",
    "dir-source",
    "vote-digest",
    "shared-rand-participate",
    "shared-rand-commit",
    "dir-key-certificate-version",
    "dir-key-published",
    "dir-key-expires",
    "dir-identity-key",
    "dir-signing-key",
    "dir-key-crosscert",
    "dir-key-certification",

    // Keywords found in consensus and vote footers.
    "directory-footer",
    "bandwidth-weights",
    "directory-signature",
];

// // Items included in consenus and vote headers.
// pub const HEADER_KEYWORDS: [&'static str; 32] = [
//     "@type",
//     "network-status-version",
//     "vote-status",
//     "consensus-method" ,
//     "valid-after",
//     "fresh-until",
//     "valid-until",
//     "voting-delay",
//     "client-versions",
//     "server-versions",
//     "known-flags",
//     "recommended-client-protocols",
//     "recommended-relay-protocols",
//     "required-client-protocols",
//     "required-relay-protocols",
//     "params",
//     "shared-rand-previous-value",
//     "shared-rand-current-value",
//     "dir-source",
//     "contact" ,
//     "vote-digest",
//     "flag-thresholds",
//     "shared-rand-participate",
//     "shared-rand-commit",
//     "dir-key-certificate-version",
//     "fingerprint",
//     "dir-key-published",
//     "dir-key-expires",
//     "dir-identity-key",
//     "dir-signing-key",
//     "dir-key-crosscert",
//     "dir-key-certification"
// ];

// // Items included in consenus and vote footers.
// pub const FOOTER_KEYWORDS: [&'static str; 3] = [
//     "directory-footer",
//     "bandwidth-weights",
//     "directory-signature"
// ];

// Items that do not require data following the keyword.
pub const DATA_OPTIONAL_ITEMS: [&'static str; 3] = [
    "hidden-service-dir",
    "tunnelled-dir-server",
    "caches-extra-info"
];

// Item keywords that demarcate a new descriptor.
pub const DELIMITER_KEYWORDS: [&'static str; 2] = [
    "@type",
    "r"
];

// Items found in Torrc files.
pub const TORRC_KEYWORDS: [&'static str; 1] = [
    "ControlPort"
];
