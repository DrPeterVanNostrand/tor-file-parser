use descriptor::DescItem;

pub fn invalid_encoding(item: &DescItem, expected_encoding: &str) -> String {
    format!(
        "Invalid String Encoding: expected {}.\n\
        ---> {}.",
        expected_encoding,
        item
    )
}

pub fn invalid_key_length(item: &DescItem, found_length: usize, expected_length: usize) -> String {
    format!(
        "Invalid Key Length: expected {} characters, found {}.\n\
        ---> {}.",
        expected_length,
        found_length,
        item
    )
}

pub fn invalid_length(item: &DescItem, found_length: usize, expected_length: usize) -> String {
    format!(
        "Invalid String Length: expected {} characters, found {}.\n\
        ---> {}.",
        expected_length,
        found_length,
        item
    )
}

pub fn invalid_prefix(item: &DescItem, expected_prefix: &str) -> String {
    format!(
        "Malformed Multiline Object: expected prefix: `{}`.\n\
        ---> {}",
        expected_prefix,
        item
    )
}

pub fn invalid_suffix(item: &DescItem, expected_suffix: &str) -> String {
    format!(
        "Malformed Multiline Object: expected suffix: `{}`.\n\
        ---> {}",
        expected_suffix,
        item
    )
}

pub fn invalid_timestamp(item: &DescItem) -> String {
    format!("Invalid Timestamp:\n---> {}", item)
}

pub fn invalid_type(item: &DescItem, invalid_value: &str, expected_type: &str) -> String {
    format!(
        "Type Conversion Error: could not convert `{}` to {}.\n\
        ---> {}",
        invalid_value,
        expected_type,
        item
    )
}
