use std::default::Default;

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum DocumentType {
    CachedMicroDescConsensus,
    BridgeExtraInfo,
    BridgeServerDescriptor,
    UnsanitizedBridgeServerDescriptor,
    BridgeNetworkStatus,
    ExitList,
    RelayNetworkStatusConsensus,
    RelayExtraInfo,
    RelayNetworkStatusMicrodescConsensus,
    RelayMicrodescriptor,
    RelayServerDescriptor,
    RelayNetworkStatusVote,
    Torperf,
    Unknown
}

impl Default for DocumentType {
    fn default() -> Self { DocumentType::Unknown }
}

#[derive(Debug)]
pub struct DescMeta {
    pub doc_type: DocumentType,
    pub version: f32,
    pub delimiter: Option<String>,
    pub has_header: bool,
    pub has_footer: bool,
    pub footer_delimiter: Option<String>
}

impl Default for DescMeta {
    fn default() -> Self { DescMeta::unknown() }
}

impl DescMeta {
    pub fn from_str(s: &str) -> Self {
        if s.starts_with("network-status-version") {
            DescMeta::cached_micro_desc_consensus(s)
        } else if s.starts_with("@type bridge-extra-info") {
            DescMeta::bridge_extra_info(s)
        } else if s.starts_with("@type bridge-server-descriptor") {
            DescMeta::bridge_server_descriptor(s)
        } else if s.starts_with("@type bridge-network-status") {
            DescMeta::bridge_network_status(s)
        } else if s.starts_with("@type tordnsel") {
            DescMeta::exit_list(s)
        } else if s.starts_with("@type network-status-consensus-3") {
            DescMeta::relay_network_status_consensus(s)
        } else if s.starts_with("@type extra-info") {
            DescMeta::relay_extra_info(s)
        } else if s.starts_with("@type network-status-microdesc-consensus-3") {
            DescMeta::relay_network_status_microdesc_consensus(s)
        } else if s.starts_with("@type microdescriptor") {
            DescMeta::relay_microdescriptor(s)
        } else if s.starts_with("@type server-descriptor") {
            DescMeta::relay_server_descriptor(s)
        } else if s.starts_with("@type network-status-vote-3") {
            DescMeta::relay_network_status_vote(s)
        } else if s.starts_with("@type torperf") {
            DescMeta::torperf(s)
        } else if s.starts_with("@purpose bridge") {
            DescMeta::unsanitized_bridge_server_descriptor()
        } else {
            DescMeta::unknown()
        }
    }

    fn cached_micro_desc_consensus(s: &str) -> Self {
        DescMeta {
            doc_type: DocumentType::CachedMicroDescConsensus,
            version: s.split(' ').nth(1).unwrap().parse().unwrap(),
            delimiter: Some("r".to_string()),
            has_header: true,
            has_footer: true,
            footer_delimiter: Some("directory-footer".to_string())
        }
    }

    fn bridge_extra_info(s: &str) -> Self {
        DescMeta {
            doc_type: DocumentType::BridgeExtraInfo,
            version: s.split(' ').nth(2).unwrap().parse().unwrap(),
            delimiter: Some("@type".to_string()),
            has_header: false,
            has_footer: false,
            footer_delimiter: None
        }
    }

    fn bridge_server_descriptor(s: &str) -> Self {
        DescMeta {
            doc_type: DocumentType::BridgeServerDescriptor,
            version: s.split(' ').nth(2).unwrap().parse().unwrap(),
            delimiter: Some("@type".to_string()),
            has_header: false,
            has_footer: false,
            footer_delimiter: None
        }
    }

    fn unsanitized_bridge_server_descriptor() -> Self {
        DescMeta {
            doc_type: DocumentType::UnsanitizedBridgeServerDescriptor,
            version: 0.0,
            delimiter: Some("@purpose".to_string()),
            has_header: false,
            has_footer: false,
            footer_delimiter: None
        }
    }

    fn bridge_network_status(s: &str) -> Self {
        DescMeta {
            doc_type: DocumentType::BridgeNetworkStatus,
            version: s.split(' ').nth(2).unwrap().parse().unwrap(),
            delimiter: Some("r".to_string()),
            has_header: true,
            has_footer: false,
            footer_delimiter: None
        }
    }

    fn exit_list(s: &str) -> Self {
        DescMeta {
            doc_type: DocumentType::ExitList,
            version: s.split(' ').nth(2).unwrap().parse().unwrap(),
            delimiter: Some("ExitNode".to_string()),
            has_header: true,
            has_footer: false,
            footer_delimiter: None
        }
    }

    fn relay_network_status_consensus(s: &str) -> Self {
        DescMeta {
            doc_type: DocumentType::RelayNetworkStatusConsensus,
            version: s.split(' ').nth(2).unwrap().parse().unwrap(),
            delimiter: Some("r".to_string()),
            has_header: true,
            has_footer: true,
            footer_delimiter: Some("directory-footer".to_string())
        }
    }

    fn relay_extra_info(s: &str) -> Self {
        DescMeta {
            doc_type: DocumentType::RelayExtraInfo,
            version: s.split(' ').nth(2).unwrap().parse().unwrap(),
            delimiter: Some("@type".to_string()),
            has_header: false,
            has_footer: false,
            footer_delimiter: None
        }
    }

    fn relay_network_status_microdesc_consensus(s: &str) -> Self {
        DescMeta {
            doc_type: DocumentType::RelayNetworkStatusMicrodescConsensus,
            version: s.split(' ').nth(2).unwrap().parse().unwrap(),
            delimiter: Some("r".to_string()),
            has_header: true,
            has_footer: true,
            footer_delimiter: Some("directory-footer".to_string())
        }
    }

    fn relay_microdescriptor(s: &str) -> Self {
        DescMeta {
            doc_type: DocumentType::RelayMicrodescriptor,
            version: s.split(' ').nth(2).unwrap().parse().unwrap(),
            delimiter: Some("@type".to_string()),
            has_header: false,
            has_footer: false,
            footer_delimiter: None
        }
    }

    fn relay_server_descriptor(s: &str) -> Self {
        DescMeta {
            doc_type: DocumentType::RelayServerDescriptor,
            version: s.split(' ').nth(2).unwrap().parse().unwrap(),
            delimiter: Some("@type".to_string()),
            has_header: false,
            has_footer: false,
            footer_delimiter: None
        }
    }

    fn relay_network_status_vote(s: &str) -> Self {
        DescMeta {
            doc_type: DocumentType::RelayNetworkStatusVote,
            version: s.split(' ').nth(2).unwrap().parse().unwrap(),
            delimiter: Some("r".to_string()),
            has_header: true,
            has_footer: true,
            footer_delimiter: Some("directory-footer".to_string())
        }
    }

    fn torperf(s: &str) -> Self {
        DescMeta {
            doc_type: DocumentType::Torperf,
            version: s.split(' ').nth(2).unwrap().parse().unwrap(),
            delimiter: Some("@type".to_string()),
            has_header: false,
            has_footer: false,
            footer_delimiter: None
        }
    }

    pub fn unknown() -> Self {
        DescMeta {
            doc_type: DocumentType::Unknown,
            version: 0.0,
            delimiter: None,
            has_header: false,
            has_footer: false,
            footer_delimiter: None
        }
    }
}
