use std::fmt::{self, Formatter, Display};

/// An object representing a single item from a Tor descriptor file.
#[derive(Clone, Debug, PartialEq)]
pub struct DescItem(String);

impl DescItem {
    pub fn new<T>(s: T) -> Self
        where T: AsRef<str> + Into<String>
    {
        DescItem(s.as_ref().trim().into())
    }

    pub fn from_lines(lines: Vec<String>) -> Self {
        let joined = lines.join("\n");
        DescItem::new(joined)
    }

    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }

    pub fn keyword(&self) -> &str {
        self.0.split_whitespace().nth(0).unwrap()
    }

    pub fn data(&self) -> &str {
        let keyword = self.keyword();
        self.0.trim_left_matches(keyword).trim()
    }

    pub fn has_data(&self) -> bool {
        !self.data().is_empty()
    }
}

impl Display for DescItem {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.as_str())
    }
}
