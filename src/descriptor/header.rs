use std::collections::HashMap;
use std::io::Read;

use chrono::{DateTime, Utc};

use descriptor::{DescItem, DescMeta, DocumentType};
use iter::ItemIter;
use keywords::ALL_KEYWORDS;
use log::log_unparsed_item;
use parsers::*;
use utils::is_delimiter_item;

/// An object representing a single Tor consenus or vote header (the
/// dir-spec calls this the "preamble"). Contains any data parsed prior to
/// the first descriptor in a descriptor file.
#[derive(Debug, Default, PartialEq)]
pub struct Header {
    pub doc_type: DocumentType,
    pub doc_version: f32,
    pub network_status_version: Option<u8>,
    pub vote_status: Option<VoteStatus>,
    pub consensus_methods: Vec<u8>,
    pub valid_after: Option<DateTime<Utc>>,
    pub fresh_until: Option<DateTime<Utc>>,
    pub valid_until: Option<DateTime<Utc>>,
    pub voting_delay: Option<VotingDelay>,
    pub client_versions: Option<Vec<String>>,
    pub server_versions: Option<Vec<String>>,
    pub known_flags: Option<Vec<StatusFlag>>,
    pub recommended_client_protocols: Option<HashMap<Proto, String>>,
    pub recommended_relay_protocols: Option<HashMap<Proto, String>>,
    pub required_client_protocols: Option<HashMap<Proto, String>>,
    pub required_relay_protocols: Option<HashMap<Proto, String>>,
    pub params: Option<HashMap<String, u32>>,
    pub shared_rand_previous_value: Option<SharedRandomValue>,
    pub shared_rand_current_value: Option<SharedRandomValue>,
    pub dir_sources: Option<Vec<DirSource>>,
    pub flag_thresholds: Option<FlagThresholds>,
    pub shared_rand_participate: bool,
    pub shared_rand_commits: Option<Vec<SharedRandomCommit>>,
    pub dir_key_certificate_version: Option<u8>,
    pub fingerprint: Option<String>,
    pub dir_key_published: Option<DateTime<Utc>>,
    pub dir_key_expires: Option<DateTime<Utc>>,
    pub dir_identity_key: Option<String>,
    pub dir_signing_key: Option<String>,
    pub dir_key_crosscert: Option<String>,
    pub dir_key_certification: Option<String>,
}

impl Header {
    pub fn empty() -> Self {
        Header { consensus_methods: vec![1], ..Default::default() }
    }

    pub fn is_empty(&self) -> bool {
        *self == Header::empty()
    }

    pub fn from_reader<R: Read>(readable: R) -> Self {
        let items = ItemIter::from_reader(readable, &ALL_KEYWORDS);
        let mut header = Header::empty();
        let mut on_first_item = true;

        for item in items {
            if is_delimiter_item(&item) && !on_first_item { break; }
            header.update(&item);
            on_first_item = false;
        }

        header
    }

    pub fn update(&mut self, item: &DescItem) {
        match item.keyword() {
            "@type" => {
                let DescMeta { doc_type, version, .. } = DescMeta::from_str(item.as_str());
                self.doc_type = doc_type;
                self.doc_version = version;
            },
            "network-status-version" => {
                let DescMeta { doc_type, version, .. } = DescMeta::from_str(item.as_str());
                let network_status_version = version as u8;

                // If this item is the first in the document, set
                // document-type to `CachedMicroDescConsensus`.
                if self.is_empty() {
                    self.doc_type = doc_type;
                    self.doc_version = version;
                }

                self.network_status_version = Some(network_status_version);
            },
            "vote-status" =>
                self.vote_status = parse_vote_status(item).ok(),
            "consensus-method" =>
                self.consensus_methods = parse_consensus_methods(item),
            "valid-after" =>
                self.valid_after = parse_published(item).ok(),
            "fresh-until" =>
                self.fresh_until = parse_published(item).ok(),
            "valid-until" =>
                self.valid_until = parse_published(item).ok(),
            "voting-delay" =>
                self.voting_delay = Some(parse_voting_delay(item)),
            "client-versions" =>
                self.client_versions = Some(parse_client_versions(item)),
            "server-versions" =>
                self.server_versions = Some(parse_server_versions(item)),
            "known-flags" => self.known_flags = Some(parse_s(item)),
            "recommended-client-protocols" =>
                self.recommended_client_protocols = Some(parse_proto(item)),
            "recommended-relay-protocols" =>
                self.recommended_relay_protocols = Some(parse_proto(item)),
            "required-client-protocols" =>
                self.required_client_protocols = Some(parse_proto(item)),
            "required-relay-protocols" =>
                self.required_relay_protocols = Some(parse_proto(item)),
            "params" =>
                self.params = Some(parse_params(item)),
            "shared-rand-previous-value" =>
                self.shared_rand_previous_value = Some(parse_shared_rand_value(item)),
            "shared-rand-current-value" =>
                self.shared_rand_current_value = Some(parse_shared_rand_value(item)),
            "dir-source" => {
                let dir_source = parse_dir_source(item);
                self.add_dir_source(dir_source);
            },
            "contact" => {
                let contact = item.data().to_string();
                self.add_contact(contact);
            },
            "vote-digest" => {
                let digest = item.data().to_string();
                self.add_vote_digest(digest);
            },
            "flag-thresholds" =>
                self.flag_thresholds = Some(parse_flag_thresholds(item)),
            "shared-rand-participate" =>
                self.shared_rand_participate = true,
            "shared-rand-commit" => {
                let shared_rand_commit = parse_shared_rand_commit(item);
                self.add_shared_rand_commit(shared_rand_commit);
            },
            "dir-key-certificate-version" => {
                let version: u8 = item.data().parse().unwrap();
                self.dir_key_certificate_version = Some(version);
            },
            "fingerprint" =>
                self.fingerprint = Some(item.data().to_string()),
            "dir-key-published" =>
                self.dir_key_published = parse_published(item).ok(),
            "dir-key-expires" =>
                self.dir_key_expires = parse_published(item).ok(),
            "dir-identity-key" =>
                self.dir_identity_key = Some(parse_dir_rsa_key(item)),
            "dir-signing-key" =>
                self.dir_signing_key = Some(parse_dir_rsa_key(item)),
            "dir-key-crosscert" =>
                self.dir_key_crosscert = Some(parse_dir_key_crosscert(item)),
            "dir-key-certification" =>
                self.dir_key_certification = Some(parse_dir_key_certification(item)),
            _ => log_unparsed_item(item)
        }
    }

    fn add_dir_source(&mut self, dir_source: DirSource) {
        if let Some(ref mut dir_sources) = self.dir_sources {
            dir_sources.push(dir_source);
        } else {
            self.dir_sources = Some(vec![dir_source]);
        }
    }

    fn add_contact(&mut self, contact: String) {
        if let Some(ref mut dir_sources) = self.dir_sources {
            let last = dir_sources.len() - 1;
            dir_sources[last].contact = contact;
        }
    }

    fn add_vote_digest(&mut self, vote_digest: String) {
        if let Some(ref mut dir_sources) = self.dir_sources {
            let last = dir_sources.len() - 1;
            dir_sources[last].vote_digest = Some(vote_digest);
        }
    }

    fn add_shared_rand_commit(&mut self, shared_rand_commit: SharedRandomCommit) {
        if let Some(ref mut commits) = self.shared_rand_commits {
            commits.push(shared_rand_commit);
        } else {
            self.shared_rand_commits = Some(vec![shared_rand_commit]);
        }
    }
}

#[test]
fn test_header_from_reader() {
    let bytes =
        "@type network-status-consensus-3 1.0\n\
        network-status-version 3\n\
        vote-status consensus\n\
        r whatever whatever".as_bytes();

    let header = Header::from_reader(bytes);

    let expected_header = Header {
        doc_type: DocumentType::RelayNetworkStatusConsensus,
        doc_version: 1.0,
        network_status_version: Some(3),
        vote_status: Some(VoteStatus::Consensus),
        consensus_methods: vec![1],
        ..Default::default()
    };

    assert_eq!(header, expected_header);
}
