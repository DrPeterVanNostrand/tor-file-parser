use std::collections::HashMap;
use std::io::Read;

use descriptor::DescItem;
use iter::ItemIter;
use keywords::ALL_KEYWORDS;
use log::log_unparsed_item;
use parsers::{
    DirectorySignature,
    parse_bandwidth_weights,
    parse_directory_signature
};

/// An object representing a single Tor consenus or vote footer. Contains
/// any data parsed after the "directory-footer" keyword is encountered.
#[derive(Debug, Default, PartialEq)]
pub struct Footer {
    pub bandwidth_weights: Option<HashMap<String, u32>>,
    pub directory_signatures: Option<Vec<DirectorySignature>>,
}

impl Footer {
    pub fn empty() -> Self { Footer::default() }
    pub fn is_empty(&self) -> bool { *self == Footer::empty() }

    pub fn from_reader<R: Read>(readable: R) -> Self {
        let items = ItemIter::from_reader(readable, &ALL_KEYWORDS);
        let mut footer = Footer::empty();
        let mut reached_footer = false;

        for item in items {
            if item.keyword() == "directory-footer" {
                reached_footer = true;
            } else if reached_footer {
                footer.update(&item);
            }
        }

        footer
    }

    pub fn update(&mut self, item: &DescItem) {
        match item.keyword() {
            "bandwidth-weights" => {
                let weights = parse_bandwidth_weights(item);
                self.bandwidth_weights = Some(weights);
            },
            "directory-signature" => {
                let signature = parse_directory_signature(item);
                self.add_directory_signature(signature);
            },
            _ => log_unparsed_item(item)
        }
    }

    fn add_directory_signature(&mut self, signature: DirectorySignature) {
        if let Some(ref mut signatures) = self.directory_signatures {
            signatures.push(signature);
        } else {
            self.directory_signatures = Some(vec![signature]);
        }
    }
}

#[test]
fn test_footer_from_reader() {
    let bytes =
        "s Fast Guard HSDir Running Stable V2Dir Valid\n\
        v Tor 0.2.9.12\n\
        p reject 1-65535\n\
        directory-footer\n\
        bandwidth-weights Wbd=0".as_bytes();

    let footer = Footer::from_reader(bytes);

    let expected_bandwidth_weights = {
        let mut hm = HashMap::new();
        hm.insert("Wbd".to_string(), 0);
        hm
    };

    let expected_footer = Footer {
        bandwidth_weights: Some(expected_bandwidth_weights),
        directory_signatures: None
    };

    assert_eq!(footer, expected_footer);
}
