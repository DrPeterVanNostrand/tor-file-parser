use std::collections::HashMap;
use std::fmt::{self, Display, Formatter};
use std::io::Read;
use std::net::SocketAddr;

use chrono::{DateTime, Utc};

use descriptor::{DescItem, DescMeta, DocumentType};
use iter::ItemIter;
use keywords::ALL_KEYWORDS;
use log::*;
use parsers::*;
use utils::{base64_to_hex, item_requires_data};

/// An object representing a single Tor descriptor. `Descriptor` holds
/// parsed data for server and bridge-descriptor, network-status, and
/// extra-info document types.
#[derive(Clone, Debug, Default, PartialEq)]
pub struct Descriptor {
    // Maps each DocumentType found in the descriptor to its version.
    pub doc_types: HashMap<DocumentType, f32>,

    // Items that are found in multiple document types.
    pub identity_ed25519: Option<String>,
    pub master_key_ed25519: Option<String>,
    pub published: Option<DateTime<Utc>>,
    pub fingerprint: Option<String>,
    pub router_sig_ed25519: Option<String>,
    pub router_signature: Option<String>,
    pub router_digest_sha256: Option<String>,
    pub router_digest: Option<String>,
    pub family: Option<Vec<String>>,

    // Items found in server-descriptor documents.
    pub purpose: Option<String>,
    pub router: Option<Router>,
    pub or_address: Option<SocketAddr>,
    pub platform: Option<Platform>,
    pub proto: Option<HashMap<Proto, String>>,
    pub uptime: Option<u32>,
    pub bandwidth: Option<Bandwidth>,
    pub extra_info_digest: Option<ExtraInfoDigest>,
    pub onion_key: Option<String>,
    pub signing_key: Option<String>,
    pub onion_key_crosscert: Option<String>,
    pub ntor_onion_key_crosscert: Option<NtorOnionKeyCrosscert>,
    pub hidden_service_dir: Option<Vec<f32>>,
    pub hibernating: bool,
    pub contact: Option<String>,
    pub ntor_onion_key: Option<String>,
    pub accept: Option<Vec<String>>,
    pub reject: Option<Vec<String>>,
    pub ipv6_policy: Option<HashMap<String, Vec<String>>>,
    pub tunnelled_dir_server: bool,
    pub bridge_distribution_request: Option<BridgeDistributionRequest>,
    pub caches_extra_info: bool,
    pub protocols: Option<Protocols>,

    // Items found in extra-info documents.
    pub extra_info: Option<ExtraInfo>,
    pub write_history: Option<History>,
    pub read_history: Option<History>,
    pub dirreq_write_history: Option<History>,
    pub dirreq_read_history: Option<History>,
    pub geoip_db_digest: Option<String>,
    pub geoip6_db_digest: Option<String>,
    pub dirreq_stats_end: Option<TimestampAndInterval>,
    pub hidserv_stats_end: Option<TimestampAndInterval>,
    pub bridge_stats_end: Option<TimestampAndInterval>,
    pub entry_stats_end: Option<TimestampAndInterval>,
    pub cell_stats_end: Option<TimestampAndInterval>,
    pub exit_stats_end: Option<TimestampAndInterval>,
    pub dirreq_v3_ips: Option<HashMap<String, u32>>,
    pub dirreq_v3_reqs: Option<HashMap<String, u32>>,
    pub entry_ips: Option<HashMap<String, u32>>,
    pub bridge_ips: Option<HashMap<String, u32>>,
    pub dirreq_v3_resp: Option<HashMap<ResponseStatus, u32>>,
    pub dirreq_v3_direct_dl: Option<HashMap<DownloadState, u32>>,
    pub dirreq_v3_tunneled_dl: Option<HashMap<DownloadState, u32>>,
    pub bridge_ip_versions: Option<HashMap<String, u32>>,
    pub bridge_ip_transports: Option<HashMap<PluggableTransport, u32>>,
    pub hidserv_rend_relayed_cells: Option<HidservStats>,
    pub hidserv_dir_onions_seen: Option<HidservStats>,
    pub cell_processed_cells: Option<Vec<u32>>,
    pub cell_queued_cells: Option<Vec<f32>>,
    pub cell_time_in_queue: Option<Vec<u32>>,
    pub cell_circuits_per_decile: Option<u32>,
    pub exit_kibibytes_written: Option<HashMap<String, u32>>,
    pub exit_kibibytes_read: Option<HashMap<String, u32>>,
    pub exit_streams_opened: Option<HashMap<String, u32>>,
    pub conn_bi_direct: Option<ConnBiDirect>,
    pub padding_counts: Option<PaddingCounts>,
    pub transports: Option<Vec<Transport>>,

    // Items found in network-status documents.
    pub flag_thresholds: Option<FlagThresholds>,
    pub r: Option<R>,
    pub a: Option<SocketAddr>,
    pub s: Option<Vec<StatusFlag>>,
    pub w: Option<W>,
    pub p: Option<HashMap<String, String>>,
    pub m: Option<String>,
    pub v: Option<String>,
    pub pr: Option<HashMap<Proto, String>>,
    pub id: Option<Id>
}

impl Descriptor {
    pub fn empty() -> Self { Descriptor::default() }

    pub fn new<R: Read>(readable: R) -> Self {
        let mut descriptor = Descriptor::empty();

        for item in ItemIter::from_reader(readable, &ALL_KEYWORDS) {
            descriptor.update(item);
        }

        descriptor
    }

    pub fn update(&mut self, item: DescItem) {
        if !item.has_data() && item_requires_data(&item) {
            log_item_missing_data(&item);
            return;
        }

        match item.keyword() {
            "@type" | "@purpose" | "network-status-version" =>
                self.add_doc_type(&item),
            "fingerprint" => {
                let fp = parse_fingerprint(&item).unwrap();
                self.fingerprint = Some(fp);
            },
            "identity-ed25519" => {
                let cert = parse_identity_ed25519(&item);
                self.identity_ed25519 = Some(cert);
            },
            "master-key-ed25519" => {
                let key = parse_master_key_ed25519(&item).unwrap();
                self.master_key_ed25519 = Some(key);
            },
            "published" => self.published = parse_published(&item).ok(),
            "router-digest" => {
                let digest = parse_router_digest(&item).unwrap();
                self.router_digest = Some(digest);
            },
            "router-digest-sha256" => {
                let digest = parse_router_digest_sha256(&item).unwrap();
                self.router_digest_sha256 = Some(digest);
            },
            "router-sig-ed25519" => {
                let sig = parse_router_sig_ed25519(&item).unwrap();
                self.router_sig_ed25519 = Some(sig);
            },
            "router-signature" => {
                let sig = parse_router_signature(&item).unwrap();
                self.router_signature = Some(sig);
            },
            "router" => self.router = Some(parse_router(&item)),
            "platform" => self.platform = Some(parse_platform(&item)),
            "or-address" => self.or_address = Some(parse_or_address(&item)),
            "proto" => self.proto = Some(parse_proto(&item)),
            "uptime" => self.uptime = Some(parse_uptime(&item)),
            "bandwidth" => self.bandwidth = Some(parse_bandwidth(&item)),
            "extra-info-digest" => {
                let digest = parse_extra_info_digest(&item);
                self.extra_info_digest = Some(digest);
            },
            "onion-key" => self.onion_key = Some(parse_onion_key(&item)),
            "signing-key" => self.signing_key = Some(parse_signing_key(&item)),
            "onion-key-crosscert" => {
                let cert = parse_onion_key_crosscert(&item);
                self.onion_key_crosscert = Some(cert);
            },
            "ntor-onion-key-crosscert" => {
                let cert = parse_ntor_onion_key_crosscert(&item);
                self.ntor_onion_key_crosscert = Some(cert);
            },
            "family" => self.family = Some(parse_family(&item)),
            "hidden-service-dir" => {
                let versions = parse_hidden_service_dir(&item);
                self.hidden_service_dir = Some(versions);
            },
            "hibernating" => self.hibernating = parse_hibernating(&item),
            "contact" => self.contact = Some(parse_contact(&item)),
            "bridge-distribution-request" => {
                let method = parse_bridge_distribution_request(&item);
                self.bridge_distribution_request = method;
            },
            "caches-extra-info" => self.caches_extra_info = true,
            "protocols" =>
                self.protocols = Some(parse_protocols(&item)),
            "ntor-onion-key" => {
                let key = parse_ntor_onion_key(&item);
                self.ntor_onion_key = Some(key);
            },
            "accept" => self.add_exit_policy(&item),
            "reject" => self.add_exit_policy(&item),
            "ipv6-policy" =>
                self.ipv6_policy = Some(parse_ipv6_policy(&item)),
            "tunnelled-dir-server" => self.tunnelled_dir_server = true,
            "extra-info" => {
                let extra_info = parse_extra_info(&item).unwrap();
                self.extra_info = Some(extra_info);
            },
            "write-history" => {
                let history = parse_write_history(&item).unwrap();
                self.write_history = Some(history);
            },
            "read-history" => {
                let history = parse_read_history(&item).unwrap();
                self.read_history = Some(history);
            },
            "dirreq-write-history" => {
                let history = parse_dirreq_write_history(&item).unwrap();
                self.write_history = Some(history);
            },
            "dirreq-read-history" => {
                let history = parse_dirreq_read_history(&item).unwrap();
                self.read_history = Some(history);
            },
            "geoip-db-digest" => {
                let digest = parse_geoip_db_digest(&item);
                self.geoip_db_digest = Some(digest);
            },
            "geoip6-db-digest" => {
                let digest = parse_geoip6_db_digest(&item);
                self.geoip6_db_digest = Some(digest);
            },
            "dirreq-stats-end" => {
                let tai = parse_dirreq_stats_end(&item).unwrap();
                self.dirreq_stats_end = Some(tai);
            },
            "hidserv-stats-end" => {
                let tai = parse_hidserv_stats_end(&item).unwrap();
                self.hidserv_stats_end = Some(tai);
            },
            "bridge-stats-end" => {
                let tai = parse_bridge_stats_end(&item).unwrap();
                self.bridge_stats_end = Some(tai);
            },
            "entry-stats-end" => {
                let tai = parse_entry_stats_end(&item);
                self.entry_stats_end = Some(tai);
            },
            "cell-stats-end" => {
                let tai = parse_cell_stats_end(&item);
                self.cell_stats_end = Some(tai);
            },
            "exit-stats-end" => {
                let tai = parse_exit_stats_end(&item);
                self.exit_stats_end = Some(tai);
            },
            "dirreq-v3-ips" => {
                let counter = parse_dirreq_v3_ips(&item);
                self.dirreq_v3_ips = Some(counter);
            },
            "dirreq-v3-reqs" => {
                let counter = parse_dirreq_v3_reqs(&item);
                self.dirreq_v3_reqs = Some(counter);
            },
            "entry-ips" => {
                let counter = parse_entry_ips(&item);
                self.entry_ips = Some(counter);
            },
            "bridge-ips" => {
                let counter = parse_bridge_ips(&item);
                self.bridge_ips = Some(counter);
            },
            "dirreq-v3-resp" => {
                let counter = parse_dirreq_v3_resp(&item);
                self.dirreq_v3_resp = Some(counter);
            },
            "dirreq-v3-direct-dl" => {
                let counter = parse_dirreq_v3_direct_dl(&item);
                self.dirreq_v3_direct_dl = Some(counter);
            },
            "dirreq-v3-tunneled-dl" => {
                let counter = parse_dirreq_v3_tunneled_dl(&item);
                self.dirreq_v3_tunneled_dl = Some(counter);
            },
            "bridge-ip-versions" => {
                let counter = parse_bridge_ip_versions(&item);
                self.bridge_ip_versions = Some(counter);
            },
            "bridge-ip-transports" => {
                let counter = parse_bridge_ip_transports(&item);
                self.bridge_ip_transports = Some(counter);
            },
            "hidserv-rend-relayed-cells" => {
                let hs_stats = parse_hidserv_rend_relayed_cells(&item);
                self.hidserv_rend_relayed_cells = Some(hs_stats);
            },
            "hidserv-dir-onions-seen" => {
                let hs_stats = parse_hidserv_dir_onions_seen(&item);
                self.hidserv_dir_onions_seen = Some(hs_stats);
            },
            "cell-processed-cells" => {
                let cells = parse_cell_processed_cells(&item);
                self.cell_processed_cells = Some(cells);
            },
            "cell-queued-cells" => {
                let cells = parse_cell_queued_cells(&item);
                self.cell_queued_cells = Some(cells);
            },
            "cell-time-in-queue" => {
                let cells = parse_cell_time_in_queue(&item);
                self.cell_time_in_queue = Some(cells);
            },
            "cell-circuits-per-decile" =>
                self.cell_circuits_per_decile = item.data().parse().ok(),
            "exit-kibibytes-written" => {
                let bytes_written = parse_exit_kibibytes_written(&item);
                self.exit_kibibytes_written = Some(bytes_written);
            },
            "exit-kibibytes-read" => {
                let bytes_read = parse_exit_kibibytes_read(&item);
                self.exit_kibibytes_read = Some(bytes_read);
            },
            "exit-streams-opened" => {
                let streams_opened = parse_exit_streams_opened(&item);
                self.exit_streams_opened = Some(streams_opened);
            },
            "conn-bi-direct" => {
                let conn_bi_direct = parse_conn_bi_direct(&item);
                self.conn_bi_direct = Some(conn_bi_direct);
            },
            "padding-counts" => {
                let padding_counts = parse_padding_counts(&item);
                self.padding_counts = Some(padding_counts);
            },
            "transport" => {
                if let Ok(transport) = parse_transport(&item) {
                    self.add_transport(transport);
                } else {
                    log_unrecognized_pluggable_transport(&item);
                }
            },
            "flag-thresholds" => {
                let flag_thresholds = parse_flag_thresholds(&item);
                self.flag_thresholds = Some(flag_thresholds);
            },
            "r" => self.r = Some(parse_r(&item)),
            "a" => self.a = Some(parse_or_address(&item)),
            "s" => self.s = Some(parse_s(&item)),
            "w" => self.w = Some(parse_w(&item)),
            "p" => self.p = Some(parse_p(&item)),
            "m" => self.m = Some(parse_m(&item)),
            "v" => self.v = Some(parse_v(&item)),
            "pr" => self.pr = Some(parse_pr(&item)),
            "id" => self.id = parse_id(&item).ok(),
            _ => log_unparsed_item(&item)
        };
    }

    fn add_doc_type(&mut self, item: &DescItem) {
        let DescMeta { doc_type, version, .. } = DescMeta::from_str(item.as_str());
        self.doc_types.insert(doc_type, version);
    }

    fn add_exit_policy(&mut self, item: &DescItem) {
        let keyword = item.keyword();
        let pat = item.data().to_string();

        if keyword == "accept" {
            if let Some(ref mut v) = self.accept {
                v.push(pat);
            } else {
                self.accept = Some(vec![pat]);
            }
        } else if keyword == "reject" {
            if let Some(ref mut v) = self.accept {
                v.push(pat);
            } else {
                self.accept = Some(vec![pat]);
            }
        }
    }

    fn add_transport(&mut self, transport: Transport) {
        if let Some(ref mut v) = self.transports {
            v.push(transport);
        } else {
            self.transports = Some(vec![transport]);
        }
    }

    pub fn is_empty(&self) -> bool {
        *self == Descriptor::empty()
    }

    pub fn is_exit(&self) -> bool {
        if let Some(ref flags) = self.s {
            flags.contains(&StatusFlag::Exit)
        } else {
            false
        }
    }

    pub fn is_guard(&self) -> bool {
        if let Some(ref flags) = self.s {
            flags.contains(&StatusFlag::Guard)
        } else {
            false
        }
    }

    /// Returns true if this OR has the "Fast" status flag.
    pub fn is_fast(&self) -> bool {
        if let Some(ref flags) = self.s {
            flags.contains(&StatusFlag::Fast)
        } else {
            false
        }
    }

    /// Returns true if this OR has the "Running" status flag.
    pub fn is_running(&self) -> bool {
        if let Some(ref flags) = self.s {
            flags.contains(&StatusFlag::Running)
        } else {
            false
        }
    }

    /// Returns true if this OR has the "Stable" status flag.
    pub fn is_stable(&self) -> bool {
        if let Some(ref flags) = self.s {
            flags.contains(&StatusFlag::Running)
        } else {
            false
        }
    }

    /// Converts an OR's base64 encoded identity to a hex encoded fingerprint.
    pub fn get_fingerprint(&self) -> Option<String> {
        if self.fingerprint.is_some() {
            return self.fingerprint.clone();
        }

        if let Some(ref r) = self.r {
            base64_to_hex(&r.identity).ok()
        } else {
            None
        }
    }

    pub fn get_nick(&self) -> Option<String> {
        if let Some(ref router) = self.router {
            Some(router.nick.clone())
        } else if let Some(ref r) = self.r {
            Some(r.nick.clone())
        } else if let Some(ref extra_info) = self.extra_info {
            Some(extra_info.nick.clone())
        } else {
            None
        }
    }
}

impl Display for Descriptor {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{:#?}", self)
    }
}

#[test]
fn test_descriptor() {
    use utils::datestring_to_datetime;

    let bytes =
        "@type bridge-extra-info 1.3\n\
        extra-info Unnamed 25830DD5760FCBB053231EBB31AC61C564225784\n\
        master-key-ed25519 j7qdS7p19vxRTaPEfrPh6Aj323fV1FBjFWFXb/NffJ4\n\
        published 2017-11-04 10:08:24".as_bytes();

    let desc = Descriptor::new(bytes);

    let expected_doc_types = {
        let mut hm = HashMap::new();
        hm.insert(DocumentType::BridgeExtraInfo, 1.3);
        hm
    };

    let expected_extra_info = ExtraInfo {
        nick: "Unnamed".to_string(),
        fp: "25830DD5760FCBB053231EBB31AC61C564225784".to_string()
    };

    let expected_master_key = "j7qdS7p19vxRTaPEfrPh6Aj323fV1FBjFWFXb/NffJ4".to_string();
    let expected_published = datestring_to_datetime("2017-11-04 10:08:24").unwrap();

    assert_eq!(desc.doc_types, expected_doc_types);
    assert_eq!(desc.extra_info, Some(expected_extra_info));
    assert_eq!(desc.master_key_ed25519, Some(expected_master_key));
    assert_eq!(desc.published, Some(expected_published));
}
