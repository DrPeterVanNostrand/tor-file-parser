use std::collections::HashMap;
use std::net::SocketAddr;
use std::str::FromStr;

use descriptor::DescItem;
use parsers::{
    parse_ed25519_cert,
    parse_pem_object,
    parse_rsa_public_key
};

#[derive(Clone, Debug, PartialEq)]
pub struct Bandwidth {
    pub avg: u32,
    pub burst: u32,
    pub observed: u32
}

#[derive(Clone, Debug, PartialEq)]
pub struct ExtraInfoDigest {
    pub sha1: String,
    pub sha256: Option<String>
}

#[derive(Clone, Debug, PartialEq)]
pub struct Platform {
    pub tor_version: String,
    pub os: String
}

#[derive(Clone, Debug, PartialEq)]
pub struct Router {
    pub nick: String,
    pub ip: String,
    pub or_port: Option<u16>,
    pub socks_port: Option<u16>,
    pub dir_port: Option<u16>
}

#[derive(Clone, Debug, PartialEq)]
pub struct NtorOnionKeyCrosscert {
    pub bit: u8,
    pub cert: String
}

#[derive(Clone, Debug, PartialEq)]
pub enum BridgeDistributionRequest {
    Any,
    Https,
    Email,
    Moat,
    Hyphae
}

impl FromStr for BridgeDistributionRequest {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let req = match s {
            "any" => BridgeDistributionRequest::Any,
            "https" => BridgeDistributionRequest::Https,
            "email" => BridgeDistributionRequest::Email,
            "moat" => BridgeDistributionRequest::Moat,
            "hyphae" => BridgeDistributionRequest::Hyphae,
            _ => return Err(())
        };
        Ok(req)
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct Protocols {
    pub link_versions: Vec<u8>,
    pub circuit_versions: Vec<u8>
}

pub fn parse_bandwidth(item: &DescItem) -> Bandwidth {
    let split: Vec<_> = item.data().split(' ').collect();
    let avg = split[0].parse().unwrap();
    let burst = split[1].parse().unwrap();
    let observed = split[2].parse().unwrap();
    Bandwidth { avg, burst, observed }
}

pub fn parse_contact(item: &DescItem) -> String {
    item.data().to_string()
}

pub fn parse_bridge_distribution_request(item: &DescItem) -> Option<BridgeDistributionRequest> {
    BridgeDistributionRequest::from_str(item.data()).ok()
}

pub fn parse_extra_info_digest(item: &DescItem) -> ExtraInfoDigest {
    let mut split = item.data().split(' ');
    let sha1 = split.next().unwrap().to_string();
    let sha256 = split.next().map(|sha256| sha256.to_string());
    ExtraInfoDigest { sha1, sha256 }
}

pub fn parse_hidden_service_dir(item: &DescItem) -> Vec<f32> {
    item.data().split(' ')
        .filter_map(|v| v.parse().ok())
        .collect()
}

pub fn parse_hibernating(item: &DescItem) -> bool {
    item.data() == "1"
}

pub fn parse_ntor_onion_key(item: &DescItem) -> String {
    item.data().to_string()
}

pub fn parse_platform(item: &DescItem) -> Platform {
    let split: Vec<_> = item.data().splitn(4, ' ').collect();
    let tor_version = split[1].to_string();
    let os = split[3].to_string();
    Platform { tor_version, os }
}

pub fn parse_protocols(item: &DescItem) -> Protocols {
    let split: Vec<_> = item.data().trim_left_matches("Link ")
        .split("Circuit")
        .collect();

    let link_versions: Vec<u8> = split[0].trim().split(' ')
        .map(|x| x.parse().unwrap())
        .collect();

    let circuit_versions: Vec<u8> = split[1].trim().split(' ')
        .map(|x| x.parse().unwrap())
        .collect();

    Protocols { link_versions, circuit_versions }
}

pub fn parse_ipv6_policy(item: &DescItem) -> HashMap<String, Vec<String>> {
    let split: Vec<_> = item.data().split(' ').collect();
    let mut hm = HashMap::new();

    let pats = split[1].split(',')
        .map(|pat| pat.to_string())
        .collect();

    if split[0] == "accept" {
        hm.insert("accept".to_string(), pats);
    } else if split[0] == "reject" {
        hm.insert("reject".to_string(), pats);
    }

    hm
}

pub fn parse_router(item: &DescItem) -> Router {
    let split: Vec<_> = item.data().split(' ').collect();
    let nick = split[0].to_string();
    let ip = split[1].to_string();

    let or_port: Option<u16> = if split[2] == "0" {
        None
    } else {
        Some(split[2].parse().unwrap())
    };

    let socks_port: Option<u16> = if split[3] == "0" {
        None
    } else {
        Some(split[3].parse().unwrap())
    };

    let dir_port: Option<u16> = if split[4] == "0" {
        None
    } else {
        Some(split[4].parse().unwrap())
    };

    Router { nick, ip, or_port, socks_port, dir_port }
}

pub fn parse_or_address(item: &DescItem) -> SocketAddr {
    item.data().parse().unwrap()
}

pub fn parse_uptime(item: &DescItem) -> u32 {
    item.data().parse().unwrap()
}

pub fn parse_signing_key(item: &DescItem) -> String {
    parse_rsa_public_key(item).unwrap()
}

pub fn parse_onion_key(item: &DescItem) -> String {
    parse_rsa_public_key(item).unwrap()
}

pub fn parse_onion_key_crosscert(item: &DescItem) -> String {
    let prefix = "-----BEGIN CROSSCERT-----";
    let suffix = "-----END CROSSCERT-----";
    parse_pem_object(item, prefix, suffix).unwrap()
}

pub fn parse_ntor_onion_key_crosscert(item: &DescItem) -> NtorOnionKeyCrosscert {
    let bit: u8 = item.data().lines().nth(0).unwrap().parse().unwrap();
    let cert = parse_ed25519_cert(item);
    NtorOnionKeyCrosscert { bit, cert }
}

#[test]
fn test_parse_bandwidth() {
    let s = "bandwidth 1 2 3";
    let item = DescItem::new(s);
    let expected = Bandwidth { avg: 1, burst: 2, observed: 3 };
    assert_eq!(parse_bandwidth(&item), expected);
}

#[test]
fn test_parse_platform() {
    let s = "platform Tor 0.3.0.3-alpha on Windows 8";
    let item = DescItem::new(s);
    let expected = Platform {
        tor_version: "0.3.0.3-alpha".to_string(),
        os: "Windows 8".to_string()
    };
    assert_eq!(parse_platform(&item), expected);
}

#[test]
fn test_parse_router() {
    let s = "router Unknown 10.10.10.10 9001 0 9030";
    let item = DescItem::new(s);
    let expected = Router {
        nick: "Unknown".to_string(),
        ip: "10.10.10.10".to_string(),
        or_port: Some(9001),
        socks_port: None,
        dir_port: Some(9030)
    };
    assert_eq!(parse_router(&item), expected);
}

#[test]
fn test_onion_key_crosscert() {
    let s =
        "onion-key-crosscert\n\
        -----BEGIN CROSSCERT-----\n\
        KNNt8Sn/FHDGwcZVvlvUvTvzigvclUoKndsgQbWcHPCOUCbdW0d/+v5oZlZXpXMZ\n\
        wtl+UXmx23XjwW4m0c4pe97Lwh9BYnjAadd0A4wissgXC/iq6luKPXVuYzI9/JDE\n\
        K+gofN3XSiwudXeeStIcuWMHWkGRFtxrlfIfL5y01dY=\n\
        -----END CROSSCERT-----";

    let item = DescItem::new(s);

    let expected =
        "KNNt8Sn/FHDGwcZVvlvUvTvzigvclUoKndsgQbWcHPCOUCbdW0d/+v5oZlZXpXMZ\
        wtl+UXmx23XjwW4m0c4pe97Lwh9BYnjAadd0A4wissgXC/iq6luKPXVuYzI9/JDE\
        K+gofN3XSiwudXeeStIcuWMHWkGRFtxrlfIfL5y01dY=".to_string();

    assert_eq!(parse_onion_key_crosscert(&item), expected);
}

#[test]
fn test_ntor_onion_key_crosscert() {
    let s =
        "ntor-onion-key-crosscert 1\n\
        -----BEGIN ED25519 CERT-----\n\
        AQoABm8RAadboYJDZdAJvTvRthiFaXpBJNblVMsJjvC03/qrOg6nACHre7jjKSo9\n\
        Ib98jC/18j7RfIy35o5AeiMxZrl7oEW2xDuCZ/xppjpweFSkvgzEFfSkK/oc8AGq\n\
        HMnIzwOMegI=\n\
        -----END ED25519 CERT-----";

    let item = DescItem::new(s);

    let expected_cert =
        "AQoABm8RAadboYJDZdAJvTvRthiFaXpBJNblVMsJjvC03/qrOg6nACHre7jjKSo9\
        Ib98jC/18j7RfIy35o5AeiMxZrl7oEW2xDuCZ/xppjpweFSkvgzEFfSkK/oc8AGq\
        HMnIzwOMegI=".to_string();

    let expected = NtorOnionKeyCrosscert { bit: 1, cert: expected_cert};
    assert_eq!(parse_ntor_onion_key_crosscert(&item), expected);
}
