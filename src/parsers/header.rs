use std::collections::HashMap;
use std::str::FromStr;

use descriptor::DescItem;
use log::log_type_conversion_error;
use parsers::parse_pem_object;

#[derive(Debug, PartialEq)]
pub enum VoteStatus {
    Consensus,
    Vote
}

impl FromStr for VoteStatus {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let status = match s {
            "consensus" => VoteStatus::Consensus,
            "vote" => VoteStatus::Vote,
            _ => return Err(())
        };

        return Ok(status)
    }
}

#[derive(Debug, PartialEq)]
pub struct VotingDelay {
    pub vote_seconds: u32,
    pub dist_seconds: u32
}

#[derive(Debug, PartialEq)]
pub struct SharedRandomCommit {
    pub version: u8,
    pub alg: String,
    pub identity: String,
    pub commit: String,
    pub reveal: Option<String>
}

#[derive(Debug, PartialEq)]
pub struct SharedRandomValue {
    pub n_reveals: u32,
    pub value: String
}

#[derive(Debug, PartialEq)]
pub struct DirSource {
    pub nick: String,
    pub identity: String,
    pub addr: String,
    pub ip: String,
    pub dir_ports: Vec<u16>,
    pub contact: String,
    pub vote_digest: Option<String>,
}

pub fn parse_vote_status(item: &DescItem) -> Result<VoteStatus, ()> {
    let vote_status_res = VoteStatus::from_str(item.data());
    if vote_status_res.is_err() {
        log_type_conversion_error(&item, item.data(), "VoteStatus");
    }
    vote_status_res
}

pub fn parse_consensus_methods(item: &DescItem) -> Vec<u8> {
    item.data().split(' ')
        .filter_map(|version| version.parse().ok())
        .collect()
}

pub fn parse_voting_delay(item: &DescItem) -> VotingDelay {
    let split: Vec<_> = item.data().split(' ').collect();
    let vote_seconds: u32 = split[0].parse().unwrap();
    let dist_seconds: u32 = split[0].parse().unwrap();
    VotingDelay { vote_seconds, dist_seconds }
}

pub fn parse_client_versions(item: &DescItem) -> Vec<String> {
    item.data().split(',')
        .map(|version| version.to_string())
        .collect()
}

pub fn parse_server_versions(item: &DescItem) -> Vec<String> {
    item.data().split(',')
        .map(|version| version.to_string())
        .collect()
}

pub fn parse_params(item: &DescItem) -> HashMap<String, u32> {
    item.data().split(' ')
        .map(|pair| {
            let mut split = pair.splitn(2, '=');
            let key = split.next().unwrap().to_string();
            let value: u32 = split.next().unwrap().parse().unwrap();
            (key, value)
        })
        .collect()
}

pub fn parse_shared_rand_commit(item: &DescItem) -> SharedRandomCommit {
    let split: Vec<_> = item.data().split(' ').collect();
    let version: u8 = split[0].parse().unwrap();
    let alg = split[1].to_string();
    let identity = split[2].to_string();
    let commit = split[3].to_string();

    let reveal = if split.len() > 4 {
        Some(split[4].to_string())
    } else {
        None
    };

    SharedRandomCommit { version, alg, identity, commit, reveal }
}

pub fn parse_shared_rand_value(item: &DescItem) -> SharedRandomValue {
    let split: Vec<_> = item.data().split(' ').collect();
    let n_reveals: u32 = split[0].parse().unwrap();
    let value = split[1].to_string();
    SharedRandomValue { n_reveals, value }
}

pub fn parse_dir_source(item: &DescItem) -> DirSource {
    let split: Vec<_> = item.data().splitn(5, ' ').collect();
    let nick = split[0].to_string();
    let identity = split[1].to_string();
    let addr = split[2].to_string();
    let ip = split[3].to_string();

    let dir_ports = split[4].split(' ')
        .map(|port| port.parse().unwrap())
        .collect();

    let contact = String::new();
    let vote_digest = None;
    DirSource { nick, identity, addr, ip, dir_ports, contact, vote_digest }
}

pub fn parse_dir_rsa_key(item: &DescItem) -> String {
    let prefix = "-----BEGIN RSA PUBLIC KEY-----";
    let suffix = "-----END RSA PUBLIC KEY-----";
    parse_pem_object(item, prefix, suffix).unwrap()
}

pub fn parse_dir_key_crosscert(item: &DescItem) -> String {
    let prefix = "-----BEGIN ID SIGNATURE-----";
    let suffix = "-----END ID SIGNATURE-----";
    parse_pem_object(item, prefix, suffix).unwrap()
}

pub fn parse_dir_key_certification(item: &DescItem) -> String {
    let prefix = "-----BEGIN SIGNATURE-----";
    let suffix = "-----END SIGNATURE-----";
    parse_pem_object(item, prefix, suffix).unwrap()
}
