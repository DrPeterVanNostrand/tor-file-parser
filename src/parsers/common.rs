use std::collections::HashMap;
use std::str::FromStr;

use chrono::{DateTime, Utc};

use descriptor::DescItem;
use errors::*;
use log::log_type_conversion_error;
use utils::*;

const ED25519_KEY_LENGTH: usize = 43;

// The length (in characters) of a base64 encoded ED25519 signature
// (with the trailing "=" removed).
const ED25519_SIG_LENGTH: usize = 86;

// The length in hex characters of a fingerprint.
pub const FINGERPRINT_LENGTH: usize = 40;

const ROUTER_DIGEST_LENGTH: usize = 40;

// The length (in characters) of a base64 encoded Router Signature
// (containing the trailing "=").
const ROUTER_SIGNATURE_LENGTH: usize = 172;

// The length (in characters) of a base64 encoded RSA public key
// (containing the trailing "=").
const RSA_PUBLIC_KEY_LENGTH: usize = 188;

// The length (in characters) of a base64 encoded SHA256 digest
// (with the trailing "=" removed).
const SHA256_BASE64_LENGTH: usize = 43;

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum Proto {
    Cons,
    Desc,
    DirCache,
    HSDir,
    HSIntro,
    HSRend,
    Link,
    LinkAuth,
    Microdesc,
    Relay
}

impl FromStr for Proto {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let entry = match s {
            "Cons" => Proto::Cons,
            "Desc" => Proto::Desc,
            "DirCache" => Proto::DirCache,
            "HSDir" => Proto::HSDir,
            "HSIntro" => Proto::HSIntro,
            "HSRend" => Proto::HSRend,
            "Link" => Proto::Link,
            "LinkAuth" => Proto::LinkAuth,
            "Microdesc" => Proto::Microdesc,
            "Relay" => Proto::Relay,
            _ => return Err(())
        };
        Ok(entry)
    }
}

/// Parse a multiline PEM encoded "object" as defined by
/// [section 1.2 of the dir-spec]
/// (https://gitweb.torproject.org/torspec.git/tree/dir-spec.txt).
pub fn parse_pem_object(item: &DescItem, prefix: &str, suffix: &str) -> Result<String, String> {
    let mut lines: Vec<_> = item.data().lines().collect();

    // Strip the leading `BIT` argument from any "ntor-onion-key-crosscert" items.
    if item.keyword() == "ntor-onion-key-crosscert" ||
       item.keyword() == "directory-signature"
    {
        lines.remove(0);
    }

    if lines[0] != prefix {
        return Err(invalid_prefix(&item, prefix));
    }

    let last = lines.len() - 1;

    if lines[last] != suffix {
        return Err(invalid_suffix(&item, suffix));
    }

    let joined = lines[1..last].join("");

    if !is_base64_encoded(&joined) {
        return Err(invalid_encoding(&item, "base64"));
    }

    Ok(joined)
}

/// Parse a multiline PEM encoded RSA public key.
pub fn parse_rsa_public_key(item: &DescItem) -> Result<String, String> {
    let prefix = "-----BEGIN RSA PUBLIC KEY-----";
    let suffix = "-----END RSA PUBLIC KEY-----";
    let key = parse_pem_object(&item, prefix, suffix)?;
    let n_chars = key.chars().count();

    if n_chars != RSA_PUBLIC_KEY_LENGTH {
        Err(invalid_key_length(&item, n_chars, RSA_PUBLIC_KEY_LENGTH))
    } else {
        Ok(key)
    }
}

/// Parse a multiline PEM encoded ED25519 cert.
pub fn parse_ed25519_cert(item: &DescItem) -> String {
    let prefix = "-----BEGIN ED25519 CERT-----";
    let suffix = "-----END ED25519 CERT-----";
    parse_pem_object(&item, prefix, suffix).unwrap()
}

/// Parse the "fingerprint" item.
///
/// Note: there exists two versions of the "fingerprint" item. One version
/// separates the fingerprint into 10 space-separated groups of 4
/// characters. The second version expresses the fingerprint as one 40
/// character long string without spaces.
pub fn parse_fingerprint(item: &DescItem) -> Result<String, String> {
    let fp: String = item.data().split(' ').collect();
    let n_chars = fp.chars().count();

    if n_chars != 40 {
        return Err(invalid_length(&item, n_chars, FINGERPRINT_LENGTH));
    }

    if !is_hex_encoded(&fp) {
        return Err(invalid_encoding(&item, "hex"));
    }

    Ok(fp)
}

/// Parse the "published" item.
pub fn parse_published(item: &DescItem) -> Result<DateTime<Utc>, ()> {
    let split: Vec<_> = item.data().split(' ').collect();
    let datestring = &split[0..2].join(" ");
    let res = datestring_to_datetime(&datestring);
    if res.is_err() {
        log_type_conversion_error(item, &datestring, "DateTime<Utc>");
    }
    res
}

/// Parse the "master-key-ed25519" item.
pub fn parse_master_key_ed25519(item: &DescItem) -> Result<String, String> {
    let key = item.data().to_string();
    let n_chars = key.chars().count();

    if n_chars != ED25519_KEY_LENGTH {
        return Err(invalid_key_length(&item, n_chars, ED25519_KEY_LENGTH));
    }

    if !is_base64_encoded(&key) {
        return Err(invalid_encoding(&item, "base64"));
    }

    Ok(key)
}

pub fn parse_router_digest(item: &DescItem) -> Result<String, String> {
    let digest = item.data().to_string();
    let n_chars = digest.chars().count();

    if n_chars != ROUTER_DIGEST_LENGTH {
        return Err(invalid_length(&item, n_chars, ROUTER_DIGEST_LENGTH));
    }

    if !is_hex_encoded(&digest) {
        return Err(invalid_encoding(&item, "hex"));
    }

    Ok(digest)
}

pub fn parse_router_digest_sha256(item: &DescItem) -> Result<String, String> {
    let digest = item.data().trim_right_matches('=').to_string();
    let n_chars = digest.chars().count();

    if n_chars != SHA256_BASE64_LENGTH {
        return Err(invalid_length(&item, n_chars, SHA256_BASE64_LENGTH));
    }

    if !is_base64_encoded(&digest) {
        return Err(invalid_encoding(&item, "base64"));
    }

    Ok(digest)
}

pub fn parse_router_sig_ed25519(item: &DescItem) -> Result<String, String> {
    let sig = item.data().to_string();
    let n_chars = sig.chars().count();

    if n_chars != ED25519_SIG_LENGTH {
        return Err(invalid_length(&item, n_chars, ED25519_SIG_LENGTH));
    }

    if !is_base64_encoded(&sig) {
        return Err(invalid_encoding(&item, "base64"));
    }

    Ok(sig)
}

pub fn parse_identity_ed25519(item: &DescItem) -> String {
    parse_ed25519_cert(&item)
}

pub fn parse_router_signature(item: &DescItem) -> Result<String, String> {
    let prefix = "-----BEGIN SIGNATURE-----";
    let suffix = "-----END SIGNATURE-----";
    let sig = parse_pem_object(&item, prefix, suffix)?;
    let n_chars = sig.chars().count();

    if n_chars != ROUTER_SIGNATURE_LENGTH {
        return Err(invalid_length(&item, n_chars, ROUTER_SIGNATURE_LENGTH));
    }

    Ok(sig)
}

pub fn parse_proto(item: &DescItem) -> HashMap<Proto, String> {
    split_into_key_value_pairs(item.data(), ' ').into_iter()
        .filter_map(|(key, value)| {
            let proto = Proto::from_str(&key).ok()?;
            Some((proto, value))
        })
        .collect()
}

pub fn parse_family(item: &DescItem) -> Vec<String> {
    item.data().split(' ')
        .map(|family| family.to_string())
        .collect()
}

#[test]
fn test_parse_pem_object() {
    let mut s = "keyword\nAAA\none\ntwo\nZZZ";
    let mut item = DescItem::new(s);
    let mut parsed = parse_pem_object(&item, "AAA", "ZZZ");
    assert_eq!(parsed, Ok("onetwo".to_string()));

    s = "ntor-onion-key-crosscert 0\nAAA\none\ntwo\nZZZ";
    item = DescItem::new(s);
    parsed = parse_pem_object(&item, "AAA", "ZZZ");
    assert_eq!(parsed, Ok("onetwo".to_string()));
}

#[test]
fn test_parse_rsa_public_key() {
    let s =
       "onion-key\n\
        -----BEGIN RSA PUBLIC KEY-----\n\
        MIGJAoGBAKeb6Z1zHdN/Fv1Ml2mQ8d9Dt96OnDfBkJztlxAVr2LBkhh3OfAXYhNH\n\
        zn5KtAteEPC2GBmtyn+GPV+gjP5iWun8/tnJ0gbS9IJ4/MCah0RNF5oOlYTmMiXm\n\
        9z0yXEtpm15XDJ5mhncWPeZZAfyCxwHYM7tI4P7NI1SYxm4AoNHBAgMBAAE=\n\
        -----END RSA PUBLIC KEY-----";

    let item = DescItem::new(s);
    let parsed = parse_rsa_public_key(&item);

    let expected_key = String::from(
        "MIGJAoGBAKeb6Z1zHdN/Fv1Ml2mQ8d9Dt96OnDfBkJztlxAVr2LBkhh3OfAXYhNH\
        zn5KtAteEPC2GBmtyn+GPV+gjP5iWun8/tnJ0gbS9IJ4/MCah0RNF5oOlYTmMiXm\
        9z0yXEtpm15XDJ5mhncWPeZZAfyCxwHYM7tI4P7NI1SYxm4AoNHBAgMBAAE="
    );

    assert_eq!(parsed, Ok(expected_key));
}

#[test]
fn test_parse_ed25519_cert() {
    let s =
       "identity-ed25519\n\
        -----BEGIN ED25519 CERT-----\n\
        AQQABm59AWXjXQdvUJT4WDaKlGb1uhxoLsA04VlyBHb0ceyfOqj8AQAgBABhcLLB\n\
        ch5vHWVUQjbwOhxNbdCLSUK2RUYou92uX6wVPQHw9MT1yYTu1T84Rn1LPIqKsJyh\n\
        yCzMVFwd8y2SE+8T/zrXNG2i8r5uDASQC11kqkyhvZuBRNQA+nUvf62lWAI=\n\
        -----END ED25519 CERT-----";

    let item = DescItem::new(s);
    let parsed = parse_ed25519_cert(&item);

    let expected_cert = String::from(
        "AQQABm59AWXjXQdvUJT4WDaKlGb1uhxoLsA04VlyBHb0ceyfOqj8AQAgBABhcLLB\
        ch5vHWVUQjbwOhxNbdCLSUK2RUYou92uX6wVPQHw9MT1yYTu1T84Rn1LPIqKsJyh\
        yCzMVFwd8y2SE+8T/zrXNG2i8r5uDASQC11kqkyhvZuBRNQA+nUvf62lWAI="
    );

    assert_eq!(parsed, expected_cert);
}

#[test]
fn test_parse_fingerprint() {
    let mut s = "fingerprint DA7D 0E27 B08F 032C 7663 B58C 3208 5E25 29CB CDA2";
    let mut item = DescItem::new(s);
    let mut parsed = parse_fingerprint(&item);
    assert_eq!(parsed, Ok("DA7D0E27B08F032C7663B58C32085E2529CBCDA2".to_string()));

    s = "fingerprint DA7D0E27B08F032C7663B58C32085E2529CBCDA2";
    item = DescItem::new(s);
    parsed = parse_fingerprint(&item);
    assert_eq!(parsed, Ok("DA7D0E27B08F032C7663B58C32085E2529CBCDA2".to_string()));
}

#[test]
fn test_published() {
    let s = "published 2018-01-29 13:01:08";
    let item = DescItem::new(s);
    let parsed = parse_published(&item);
    let expected_dt = datestring_to_datetime("2018-01-29 13:01:08").unwrap();
    assert!(parsed.is_ok());
    assert_eq!(parsed.unwrap(), expected_dt);
}

#[test]
fn test_parse_master_key_ed25519() {
    let s = "master-key-ed25519 NPmTcHwunEr0dHSYRA0/Pt9coeibcSkuhl/kotFdyH8";
    let item = DescItem::new(s);

    assert_eq!(
        parse_master_key_ed25519(&item),
        Ok("NPmTcHwunEr0dHSYRA0/Pt9coeibcSkuhl/kotFdyH8".to_string())
    );
}

#[test]
fn test_parse_router_digest() {
    let s = "router-digest 1B3050C908B550C252A2EFFA16739FEF0AC729BA";
    let item = DescItem::new(s);

    assert_eq!(
        parse_router_digest(&item),
        Ok("1B3050C908B550C252A2EFFA16739FEF0AC729BA".to_string())
    );
}

#[test]
fn test_parse_router_digest_sha256() {
    let s = "router-digest-sha256 ublfoZmtt3JSYhNL1HJ6Gf2MtH/JXnbl7HLOclvUbk4";
    let item = DescItem::new(s);

    assert_eq!(
        parse_router_digest_sha256(&item),
        Ok("ublfoZmtt3JSYhNL1HJ6Gf2MtH/JXnbl7HLOclvUbk4".to_string())
    );
}

#[test]
fn test_parse_router_sig_ed25519() {
    let sig = "eRR1Oi6ubQ51t1Y2FCQfL0rhbgm5GRlSwQFA6WOgue7IE1+gaoFCsvZZWAT6iMwBFwBMpDMgm75g/KBXp/3MCg".to_string();
    let s = format!("router-digest-sha256 {}", sig);
    let item = DescItem::new(s);
    assert_eq!(parse_router_sig_ed25519(&item), Ok(sig));
}

#[test]
fn test_parse_router_signature() {
    let s =
       "router-signature\n\
        -----BEGIN SIGNATURE-----\n\
        KDCx452mjU2bUCJRSfiFze2l6J8JpGpO6ErGIu51Gvr3/Wos92+Uz+XQi5vVTOLC\n\
        1THBbU+Qd/zUGOPIMBuyDUJBJMsQBTj8RCj6MdpoI0COgqZpgkLP42cHVynXwu5k\n\
        8+WrSKWXQsjALeOct/q6v34nVsStQ+REmQ8X+nmeIxw=\n\
        -----END SIGNATURE-----";

    let item = DescItem::new(s);
    let parsed = parse_router_signature(&item);

    let expected_sig = String::from(
        "KDCx452mjU2bUCJRSfiFze2l6J8JpGpO6ErGIu51Gvr3/Wos92+Uz+XQi5vVTOLC\
        1THBbU+Qd/zUGOPIMBuyDUJBJMsQBTj8RCj6MdpoI0COgqZpgkLP42cHVynXwu5k\
        8+WrSKWXQsjALeOct/q6v34nVsStQ+REmQ8X+nmeIxw="
    );

    assert_eq!(parsed, Ok(expected_sig));
}

#[test]
fn test_parse_proto() {
    let s = "proto Cons=1-2 Desc=1-2 DirCache=1-2";
    let item = DescItem::new(s);
    let proto_hm = parse_proto(&item);

    let expected_proto_hm = {
        let mut hm = HashMap::new();
        hm.insert(Proto::Cons, "1-2".to_string());
        hm.insert(Proto::Desc, "1-2".to_string());
        hm.insert(Proto::DirCache, "1-2".to_string());
        hm
    };

    assert_eq!(proto_hm, expected_proto_hm);
}
