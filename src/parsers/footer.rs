use std::collections::HashMap;

use descriptor::DescItem;
use parsers::parse_pem_object;

#[derive(Debug, PartialEq)]
pub struct DirectorySignature {
    pub alg: Option<String>,
    pub identity: String,
    pub signing_key_digest: String,
    pub signature: String,
}

pub fn parse_directory_signature(item: &DescItem) -> DirectorySignature {
    let first_line = item.data().lines().nth(0).unwrap();
    let mut split: Vec<_> = first_line.split(' ').collect();

    let alg = if split.len() > 2 {
        Some(split.remove(0).to_string())
    } else {
        None
    };

    let identity = split[0].to_string();
    let signing_key_digest = split[1].to_string();

    let prefix = "-----BEGIN SIGNATURE-----";
    let suffix = "-----END SIGNATURE-----";
    let signature = parse_pem_object(item, prefix, suffix).unwrap();

    DirectorySignature { alg, identity, signing_key_digest, signature }
}

pub fn parse_bandwidth_weights(item: &DescItem) -> HashMap<String, u32> {
    item.data().split(' ')
        .map(|pair| {
            let mut split = pair.splitn(2, '=');
            let key = split.next().unwrap().to_string();
            let value: u32 = split.next().unwrap().parse().unwrap();
            (key, value)
        })
        .collect()
}

#[test]
fn test_parse_directory_signature() {
    let s =
        "directory-signature sha-256 0232AF901C31A04EE9848595AF9BB7620D4C5B2E E66AE3C828CCAA8A765620B2750DD6257C9A52D4\n\
        -----BEGIN SIGNATURE-----\n\
        sYVnNy0qQPkzP7uSbeohAbjR9UJ4sVb/REJs3RLUl3QpGjEbiA0VaUAKGn1MnQFL\n\
        VwcoxUnQmVKoTw+t00j/ViHFkCa94uIB9X6r/ADaFnHgC1Gtzn/GBKXgTz8KNz+t\n\
        UC7w0gGBUlp4negjqvvK3XM1JSU68h2shepk1qFZnLxTHr7j4iQ1Y/3h7YeS4cN9\n\
        Z8F4eL8rOtNohhrOyppqG2W0Ug4oqUdPOQwGfUrNaKc7vA9uy/5sPS6rHTc4C6br\n\
        e3M4C9xLpa/PJCAt4y8NHgzTSVVTZeAZ4tKeSnitnScQ3hOVlyuPjp2h58pgalvY\n\
        qq4OZXejoh1Llql1+hV+zg==\n\
        -----END SIGNATURE-----";

    let item = DescItem::new(s);
    let directory_signature = parse_directory_signature(&item);
    assert_eq!(directory_signature.alg, Some("sha-256".to_string()));

    assert_eq!(
        directory_signature.identity,
        "0232AF901C31A04EE9848595AF9BB7620D4C5B2E".to_string()
    );
    assert_eq!(
        directory_signature.signing_key_digest,
        "E66AE3C828CCAA8A765620B2750DD6257C9A52D4".to_string()
    );

    let expected_signature =
        "sYVnNy0qQPkzP7uSbeohAbjR9UJ4sVb/REJs3RLUl3QpGjEbiA0VaUAKGn1MnQFL\
        VwcoxUnQmVKoTw+t00j/ViHFkCa94uIB9X6r/ADaFnHgC1Gtzn/GBKXgTz8KNz+t\
        UC7w0gGBUlp4negjqvvK3XM1JSU68h2shepk1qFZnLxTHr7j4iQ1Y/3h7YeS4cN9\
        Z8F4eL8rOtNohhrOyppqG2W0Ug4oqUdPOQwGfUrNaKc7vA9uy/5sPS6rHTc4C6br\
        e3M4C9xLpa/PJCAt4y8NHgzTSVVTZeAZ4tKeSnitnScQ3hOVlyuPjp2h58pgalvY\
        qq4OZXejoh1Llql1+hV+zg==".to_string();

    assert_eq!(
        directory_signature.signature,
        expected_signature
    );
}
