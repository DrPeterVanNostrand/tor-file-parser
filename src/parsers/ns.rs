use std::collections::HashMap;
use std::str::FromStr;

use chrono::{DateTime, Utc};

use descriptor::DescItem;
use parsers::{parse_proto, Proto};
use utils::datestring_to_datetime;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct FlagThresholds {
    pub stable_uptime: Option<u32>,
    pub stable_mtbf: Option<u32>,
    pub fast_speed: Option<u32>,
    pub guard_wfu: Option<f32>,
    pub guard_tk: Option<u32>,
    pub guard_bw_inc_exits: Option<u32>,
    pub guard_bw_exc_exits: Option<u32>,
    pub enough_mtbf: Option<u32>,
    pub ignoring_advertised_bws: Option<u32>
}

#[derive(Clone, Debug, PartialEq)]
pub struct R {
    pub nick: String,
    pub identity: String,
    pub most_recent_desc_digest: Option<String>,
    pub timestamp: DateTime<Utc>,
    pub ip: String,
    pub or_port: u16,
    pub dir_port: Option<u16>
}

#[derive(Clone, Debug, PartialEq)]
pub enum StatusFlag {
    Authority,
    BadExit,
    Exit,
    Fast,
    Guard,
    HSDir,
    NoEdConsensus,
    Stable,
    Running,
    Valid,
    V2Dir
}

impl FromStr for StatusFlag {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let flag = match s {
            "Authority" => StatusFlag::Authority,
            "BadExit" => StatusFlag::BadExit,
            "Exit" => StatusFlag::Exit,
            "Fast" => StatusFlag::Fast,
            "Guard" => StatusFlag::Guard,
            "HSDir" => StatusFlag::HSDir,
            "NoEdConsensus" => StatusFlag::NoEdConsensus,
            "Stable" => StatusFlag::Stable,
            "Running" => StatusFlag::Running,
            "Valid" => StatusFlag::Valid,
            "V2Dir" => StatusFlag::V2Dir,
            _ => return Err(())
        };

        Ok(flag)
    }
}

impl StatusFlag {
    pub fn as_str(&self) -> &str {
        match *self {
            StatusFlag::Authority => "Authority",
            StatusFlag::BadExit => "BadExit",
            StatusFlag::Exit => "Exit",
            StatusFlag::Fast => "Fast",
            StatusFlag::Guard => "Guard",
            StatusFlag::HSDir => "HSDir",
            StatusFlag::NoEdConsensus => "NoEdConsensus",
            StatusFlag::Stable => "Stable",
            StatusFlag::Running => "Running",
            StatusFlag::Valid => "Valid",
            StatusFlag::V2Dir => "V2Dir",
        }
    }
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct W {
    pub bandwidth: u32,
    pub measured: Option<u32>,
    pub unmeasured: Option<u8>
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Id {
    alg: String,
    key: String
}

impl FromStr for Id {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let split: Vec<_> = s.split(' ').collect();

        if split[0] != "rsa1024" && split[0] != "ed25519" {
            return Err(());
        }

        let alg = split[0].to_string();
        let key = split[1].to_string();
        Ok(Id { alg, key })
    }
}

pub fn parse_flag_thresholds(item: &DescItem) -> FlagThresholds {
    let mut thresholds = FlagThresholds { ..Default::default() };

    for pair in item.data().split(' ') {
        let split: Vec<_> = pair.split('=').collect();
        let key = split[0];

        if key == "guard-wfu" {
            let value: f32 = split[1].trim_right_matches('%').parse().unwrap();
            thresholds.guard_wfu = Some(value);
            continue;
        }

        let value: Option<u32> = Some(split[1].parse().unwrap());

        match key {
            "stable-uptime" => thresholds.stable_uptime = value,
            "stable-mtbf" => thresholds.stable_mtbf = value,
            "fast-speed" => thresholds.fast_speed = value,
            "guard-tk" => thresholds.guard_tk = value,
            "guard-bw-inc-exits" => thresholds.guard_bw_inc_exits = value,
            "guard-bw-exc-exits" => thresholds.guard_bw_exc_exits = value,
            "enough-mtbf" => thresholds.enough_mtbf = value,
            "ignoring-advertised-bws" => thresholds.ignoring_advertised_bws = value,
            _ => continue
        };
    }

    thresholds
}

pub fn parse_r(item: &DescItem) -> R {
    let mut split: Vec<_> = item.data().split(' ').collect();
    let nick = split[0].to_string();
    let identity = split[1].to_string();

    // Consensu network-status documents do not contain a digest of their
    // most recent descriptor, all other network-status documents do
    // contain this field.
    let most_recent_desc_digest = if split.len() > 7 {
        Some(split.remove(2).to_string())
    } else {
        None
    };

    let datestring = &split[2..4].join(" ");
    let timestamp = datestring_to_datetime(&datestring).unwrap();
    let ip = split[4].to_string();
    let or_port: u16= split[5].parse().unwrap();

    let dir_port = if split[6] != "0" {
        Some(split[6].parse().unwrap())
    } else {
        None
    };

    R { nick, identity, most_recent_desc_digest, timestamp, ip, or_port, dir_port }
}

pub fn parse_s(item: &DescItem) -> Vec<StatusFlag> {
    item.data().split(' ')
        .filter_map(|flag| StatusFlag::from_str(flag).ok())
        .collect()
}

pub fn parse_p(item: &DescItem) -> HashMap<String, String> {
    let split: Vec<_> = item.data().split(' ').collect();
    let key = split[0].to_string();
    let value = split[1].to_string();
    let mut hm = HashMap::new();
    hm.insert(key, value);
    hm
}

pub fn parse_w(item: &DescItem) -> W {
    let pairs: Vec<_> = item.data().split(' ').collect();
    let bandwidth:u32 = pairs[0].split('=').nth(1).unwrap().parse().unwrap();
    let mut w = W { bandwidth, ..Default::default() };

    if pairs.len() > 1 {
        for pair in &pairs[1..] {
            let split: Vec<_> = pair.split('=').collect();
            match split[0] {
                "Measured" =>
                    w.measured = Some(split[1].parse::<u32>().unwrap()),
                "Unmeasured" =>
                    w.unmeasured = Some(split[1].parse::<u8>().unwrap()),
                _ => continue
            };
        }
    }

    w
}

pub fn parse_m(item: &DescItem) -> String {
    item.data().to_string()
}

pub fn parse_v(item: &DescItem) -> String {
    item.data().to_string()
}

pub fn parse_pr(item: &DescItem) -> HashMap<Proto, String> {
    parse_proto(item)
}

pub fn parse_id(item: &DescItem) -> Result<Id, ()> {
    Id::from_str(&item.data())
}

#[test]
fn test_parse_flag_thresholds() {
    let s = "flag-thresholds stable-uptime=1 guard-wfu=2.316%";
    let item = DescItem::new(s);
    let thresholds = parse_flag_thresholds(&item);

    let expected_thresholds = FlagThresholds {
        stable_uptime: Some(1),
        guard_wfu: Some(2.316),
        ..Default::default()
    };

    assert_eq!(thresholds, expected_thresholds);
}

#[test]
fn test_parse_r() {
    let mut s = "r Unkown AFcEo7p09hkP3jmnRwWj8U3yj08 HW4VswH31fxbT48Hi6aDyaN0ggs 2018-01-30 11:26:29 10.10.10.10 12345 0";
    let mut item = DescItem::new(s);
    let mut r = parse_r(&item);

    let mut expected_r = R {
        nick: "Unkown".to_string(),
        identity: "AFcEo7p09hkP3jmnRwWj8U3yj08".to_string(),
        most_recent_desc_digest: Some("HW4VswH31fxbT48Hi6aDyaN0ggs".to_string()),
        timestamp: datestring_to_datetime("2018-01-30 11:26:29").unwrap(),
        ip: "10.10.10.10".to_string(),
        or_port: 12345,
        dir_port: None
    };

    assert_eq!(r, expected_r);

    s = "r Unkown AFcEo7p09hkP3jmnRwWj8U3yj08 2018-01-30 11:26:29 10.10.10.10 12345 0";
    item = DescItem::new(s);
    r = parse_r(&item);
    expected_r.most_recent_desc_digest = None;
    assert_eq!(r, expected_r);
}

#[test]
fn test_parse_s() {
    let s = "s Running Stable V2Dir Valid";
    let item = DescItem::new(s);
    let status_flags = parse_s(&item);

    let expected_status_flags = vec![
        StatusFlag::Running,
        StatusFlag::Stable,
        StatusFlag::V2Dir,
        StatusFlag::Valid,
    ];

    assert_eq!(status_flags, expected_status_flags);
}

#[test]
fn test_parse_p() {
    let s = "p accept 80,443";
    let item = DescItem::new(s);
    let parsed = parse_p(&item);

    let expected_hm = {
        let mut hm = HashMap::new();
        hm.insert("accept".to_string(), "80,443".to_string());
        hm
    };

    assert_eq!(parsed, expected_hm);
}

#[test]
fn test_parse_w() {
    let s = "w Bandwidth=1000 Measured=500 Unmeasured=1";
    let item = DescItem::new(s);
    let expected_w = W {
        bandwidth: 1000,
        measured: Some(500),
        unmeasured: Some(1)
    };
    assert_eq!(parse_w(&item), expected_w);
}
