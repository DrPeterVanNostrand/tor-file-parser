use std::collections::HashMap;
use std::str::FromStr;

use chrono::{DateTime, Utc};

use descriptor::DescItem;
use errors::*;
use parsers::FINGERPRINT_LENGTH;
use utils::*;

#[derive(Clone, Debug, PartialEq)]
pub struct TimestampAndInterval {
    pub timestamp: DateTime<Utc>,
    pub interval: u32
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum DownloadState {
    Running, Complete, Timeout,
    Max, Md, Min,
    D1, D2, D3, D4, D6, D7, D8, D9,
    Q1, Q3
}

impl FromStr for DownloadState {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let dl_state = match s {
            "running" => DownloadState::Running,
            "complete" => DownloadState::Complete,
            "timeout" => DownloadState::Timeout,
            "min" => DownloadState::Min,
            "md" => DownloadState::Md,
            "max" => DownloadState::Max,
            "d1" => DownloadState::D1,
            "d2" => DownloadState::D2,
            "d3" => DownloadState::D3,
            "d4" => DownloadState::D4,
            "d6" => DownloadState::D6,
            "d7" => DownloadState::D7,
            "d8" => DownloadState::D8,
            "d9" => DownloadState::D9,
            "q1" => DownloadState::Q1,
            "q3" => DownloadState::Q3,
            _ => return Err(())
        };

        Ok(dl_state)
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct History {
    pub timestamp: DateTime<Utc>,
    pub interval: u32,
    pub bytes_list: Vec<u64>
}

#[derive(Clone, Debug, PartialEq)]
pub struct HidservStats {
    pub n_seen: i32,
    pub delta_f: u32,
    pub epsilon: f32,
    pub bin_size: u32
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum PluggableTransport {
    Fte,
    Obfs2,
    Obfs3,
    Obfs4,
    Or,
    Scramblesuit,
    Unknown,
    Websocket,
}

impl FromStr for PluggableTransport {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let flag = match s {
            "fte" => PluggableTransport::Fte,
            "obfs2" => PluggableTransport::Obfs2,
            "obfs3" => PluggableTransport::Obfs3,
            "obfs4" => PluggableTransport::Obfs4,
            "<OR>" => PluggableTransport::Or,
            "<??>" => PluggableTransport::Unknown,
            "scramblesuit" => PluggableTransport::Scramblesuit,
            "websocket" => PluggableTransport::Websocket,
            _ => return Err(())
        };

        Ok(flag)
    }
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum ResponseStatus {
    Busy,
    NotEnoughSigs,
    NotFound,
    NotModified,
    Ok,
    Unavailable
}

impl FromStr for ResponseStatus {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let status = match s {
            "busy" => ResponseStatus::Busy,
            "not-enough-sigs" => ResponseStatus::NotEnoughSigs,
            "not-found" => ResponseStatus::NotFound,
            "not-modified" => ResponseStatus::NotModified,
            "ok" => ResponseStatus::Ok,
            "unavailable" => ResponseStatus::Unavailable,
            _ => return Err(())
        };

        Ok(status)
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct ExtraInfo {
    pub nick: String,
    pub fp: String
}

#[derive(Clone, Debug, PartialEq)]
pub struct Transport {
    pub transport_type: PluggableTransport,
    pub addr: Option<String>,
    pub args: Option<String>
}

#[derive(Clone, Debug, PartialEq)]
pub struct PaddingCounts {
    pub timestamp: DateTime<Utc>,
    pub interval: u32,
    pub bin_size: Option<u64>,
    pub write_drop: Option<u64>,
    pub write_pad: Option<u64>,
    pub write_total: Option<u64>,
    pub read_drop: Option<u64>,
    pub read_pad: Option<u64>,
    pub read_total: Option<u64>,
    pub enabled_read_pad: Option<u64>,
    pub enabled_read_total: Option<u64>,
    pub enabled_write_pad: Option<u64>,
    pub enabled_write_total: Option<u64>,
    pub max_chanpad_timers: Option<u64>
}

#[derive(Clone, Debug, PartialEq)]
pub struct ConnBiDirect {
    pub timestamp: DateTime<Utc>,
    pub interval: u32,
    pub below: u32,
    pub read: u32,
    pub write: u32,
    pub both: u32
}

pub fn parse_string_u32_hashmap(item: &DescItem) -> HashMap<String, u32> {
    split_into_key_value_pairs(item.data(), ',').into_iter()
        .filter_map(|(key, value)| {
            let value = value.parse().ok()?;
            Some((key, value))
        })
        .collect()
}

pub fn parse_cc_hashmap(item: &DescItem) -> HashMap<String, u32> {
    parse_string_u32_hashmap(item).into_iter()
       .filter_map(|(key, value)|
            if key.chars().count() == 2 {
                Some((key, value))
            } else {
                None
            }
        )
        .collect()
}

pub fn parse_timestamp_and_interval(item: &DescItem) -> Result<TimestampAndInterval, String> {
    let split: Vec<_> = item.data().split(' ').collect();
    let datestring = &split[0..2].join(" ");

    let timestamp = datestring_to_datetime(&datestring)
        .map_err(|_| invalid_timestamp(&item))?;

    let interval = split[2].trim_left_matches('(').parse()
        .map_err(|_| invalid_type(&item, split[2], "u32"))?;

    let timestamp_and_interval = TimestampAndInterval { timestamp, interval };
    Ok(timestamp_and_interval)
}

pub fn parse_dl_stats(item: &DescItem) -> HashMap<DownloadState, u32> {
    parse_string_u32_hashmap(item).into_iter()
        .filter_map(|(key, value)| {
            let dl_state = DownloadState::from_str(&key).ok()?;
            Some((dl_state, value))
        })
        .collect()
}

pub fn parse_history(item: &DescItem) -> Result<History, String> {
    let TimestampAndInterval { timestamp, interval } = parse_timestamp_and_interval(item)?;

    let bytes_list: Vec<u64> = item.data().split(' ').nth(4).unwrap_or("")
        .split(',')
        .filter_map(|n_bytes| n_bytes.parse().ok())
        .collect();

    let history = History { timestamp, interval, bytes_list };
    Ok(history)
}

pub fn parse_hs_stats(item: &DescItem) -> HidservStats {
    let split: Vec<_> = item.data().splitn(2, ' ').collect();
    let n_seen: i32 = split[0].parse().unwrap();

    let hm = split_into_key_value_pairs(split[1], ' ');
    let delta_f: u32 = hm.get("delta_f").unwrap().parse().unwrap();
    let epsilon: f32 = hm.get("epsilon").unwrap().parse().unwrap();
    let bin_size: u32 = hm.get("bin_size").unwrap().parse().unwrap();

    HidservStats { n_seen, delta_f, epsilon, bin_size }
}

pub fn parse_bridge_ip_versions(item: &DescItem) -> HashMap<String, u32> {
    parse_string_u32_hashmap(&item)
}

pub fn parse_bridge_ip_transports(item: &DescItem) -> HashMap<PluggableTransport, u32> {
    parse_string_u32_hashmap(item).into_iter()
        .filter_map(|(key, value)| {
            let transport = PluggableTransport::from_str(&key).ok()?;
            Some((transport, value))
        })
        .collect()
}

pub fn parse_dirreq_v3_resp(item: &DescItem) -> HashMap<ResponseStatus, u32> {
    parse_string_u32_hashmap(item).into_iter()
        .filter_map(|(key, value)| {
            let transport = ResponseStatus::from_str(&key).ok()?;
            Some((transport, value))
        })
        .collect()
}

pub fn parse_bridge_ips(item: &DescItem) -> HashMap<String, u32> {
    parse_cc_hashmap(item)
}

pub fn parse_dirreq_v3_ips(item: &DescItem) -> HashMap<String, u32> {
    parse_cc_hashmap(item)
}

pub fn parse_dirreq_v3_reqs(item: &DescItem) -> HashMap<String, u32> {
    parse_cc_hashmap(item)
}

pub fn parse_entry_ips(item: &DescItem) -> HashMap<String, u32> {
    parse_cc_hashmap(item)
}

pub fn parse_dirreq_v3_direct_dl(item: &DescItem) -> HashMap<DownloadState, u32> {
    parse_dl_stats(item)
}

pub fn parse_dirreq_v3_tunneled_dl(item: &DescItem) -> HashMap<DownloadState, u32> {
    parse_dl_stats(item)
}

pub fn parse_read_history(item: &DescItem) -> Result<History, String> {
    parse_history(item)
}

pub fn parse_write_history(item: &DescItem) -> Result<History, String> {
    parse_history(item)
}

pub fn parse_dirreq_read_history(item: &DescItem) -> Result<History, String> {
    parse_history(item)
}

pub fn parse_dirreq_write_history(item: &DescItem) -> Result<History, String> {
    parse_history(item)
}

pub fn parse_bridge_stats_end(item: &DescItem) -> Result<TimestampAndInterval, String> {
    parse_timestamp_and_interval(item)
}

pub fn parse_dirreq_stats_end(item: &DescItem) -> Result<TimestampAndInterval, String> {
    parse_timestamp_and_interval(item)
}

pub fn parse_hidserv_stats_end(item: &DescItem) -> Result<TimestampAndInterval, String> {
    parse_timestamp_and_interval(item)
}

pub fn parse_entry_stats_end(item: &DescItem) -> TimestampAndInterval {
    parse_timestamp_and_interval(item).unwrap()
}

pub fn parse_cell_stats_end(item: &DescItem) -> TimestampAndInterval {
    parse_timestamp_and_interval(item).unwrap()
}

pub fn parse_exit_stats_end(item: &DescItem) -> TimestampAndInterval {
    parse_timestamp_and_interval(item).unwrap()
}

pub fn parse_extra_info(item: &DescItem) -> Result<ExtraInfo, String> {
    let split: Vec<_> = item.data().split(' ').collect();
    let nick = split[0].to_string();
    let fp = split[1].to_string();
    let n_chars = fp.chars().count();

    if n_chars != FINGERPRINT_LENGTH {
        return Err(invalid_length(&item, n_chars, FINGERPRINT_LENGTH));
    }

    if !is_hex_encoded(&fp) {
        return Err(invalid_encoding(&item, "hex"));
    }

    let extra_info = ExtraInfo { nick, fp };
    Ok(extra_info)
}

pub fn parse_geoip_db_digest(item: &DescItem) -> String {
    item.data().to_string()
}

pub fn parse_geoip6_db_digest(item: &DescItem) -> String {
    item.data().to_string()
}

pub fn parse_transport(item: &DescItem) -> Result<Transport, String> {
    let mut split = item.data().splitn(3, ' ');
    let transport_name = split.next().unwrap();

    let transport_type = PluggableTransport::from_str(transport_name)
        .map_err(|_| invalid_type(&item, transport_name, "PluggableTransport"))?;

    let addr = split.next().map(|addr| addr.to_string());
    let args = split.next().map(|args| args.to_string());
    let transport = Transport { transport_type, addr, args };
    Ok(transport)
}

pub fn parse_hidserv_dir_onions_seen(item: &DescItem) -> HidservStats {
    parse_hs_stats(item)
}

pub fn parse_hidserv_rend_relayed_cells(item: &DescItem) -> HidservStats {
    parse_hs_stats(item)
}

pub fn parse_cell_processed_cells(item: &DescItem) -> Vec<u32> {
    item.data().split(',')
        .filter_map(|num| num.parse().ok())
        .collect()
}

pub fn parse_cell_queued_cells(item: &DescItem) -> Vec<f32> {
    item.data().split(',')
        .filter_map(|num| num.parse().ok())
        .collect()
}

pub fn parse_cell_time_in_queue(item: &DescItem) -> Vec<u32> {
    item.data().split(',')
        .filter_map(|num| num.parse().ok())
        .collect()
}

pub fn parse_exit_kibibytes_written(item: &DescItem) -> HashMap<String, u32> {
    parse_string_u32_hashmap(&item)
}

pub fn parse_exit_kibibytes_read(item: &DescItem) -> HashMap<String, u32> {
    parse_string_u32_hashmap(&item)
}

pub fn parse_exit_streams_opened(item: &DescItem) -> HashMap<String, u32> {
    parse_string_u32_hashmap(&item)
}

pub fn parse_conn_bi_direct(item: &DescItem) -> ConnBiDirect {
    let TimestampAndInterval { timestamp, interval } =
        parse_timestamp_and_interval(&item).unwrap();

    let n_connections: Vec<_> = item.data().split(' ')
        .nth(4).unwrap()
        .split(',')
        .collect();

    let below: u32 = n_connections[0].parse().unwrap();
    let read: u32 = n_connections[1].parse().unwrap();
    let write: u32 = n_connections[2].parse().unwrap();
    let both: u32 = n_connections[3].parse().unwrap();

    ConnBiDirect { timestamp, interval, below, read, write, both }
}

pub fn parse_padding_counts(item: &DescItem) -> PaddingCounts {
    let TimestampAndInterval { timestamp, interval } =
        parse_timestamp_and_interval(&item).unwrap();

    let mut padding_counts = PaddingCounts {
        timestamp,
        interval,
        bin_size: None,
        write_drop: None,
        write_pad: None,
        write_total: None,
        read_drop: None,
        read_pad: None,
        read_total: None,
        enabled_read_pad: None,
        enabled_read_total: None,
        enabled_write_pad: None,
        enabled_write_total: None,
        max_chanpad_timers: None
    };

    for pair in item.data().split(' ').skip(4) {
        let split: Vec<_> = pair.split('=').collect();
        let key = split[0];
        let value: Option<u64> = Some(split[1].parse().unwrap());

        match key {
            "bin-size" =>
                padding_counts.bin_size = value,
            "write-drop" =>
                padding_counts.write_drop = value,
            "write-pad" =>
                padding_counts.write_pad = value,
            "write-total" =>
                padding_counts.write_total = value,
            "read-drop" =>
                padding_counts.read_drop = value,
            "read-pad" =>
                padding_counts.read_pad = value,
            "read-total" =>
                padding_counts.read_total = value,
            "enabled-read-pad" =>
                padding_counts.enabled_read_pad = value,
            "enabled-read-total" =>
                padding_counts.enabled_read_total = value,
            "enabled-write-pad" =>
                padding_counts.enabled_write_pad = value,
            "enabled-write-total" =>
                padding_counts.enabled_write_total = value,
            "max-chanpad-timers" =>
                padding_counts.max_chanpad_timers = value,
            _ => continue
        };
    }

    padding_counts
}

#[test]
fn test_parse_string_u32_hashmap() {
    let s = "dirreq-v3-resp a=1,b=2,c=xxx";
    let item = DescItem::new(s);
    let hm = parse_string_u32_hashmap(&item);

    let expected_hm = {
        let mut hm = HashMap::new();
        hm.insert("a".to_string(), 1);
        hm.insert("b".to_string(), 2);
        hm
    };

    assert_eq!(hm, expected_hm);
}

#[test]
fn test_parse_cc_hashmap() {
    let s = "dirreq-v3-resp ca=1,c=xxx,au=xxx,russia=10,ch=2";
    let item = DescItem::new(s);
    let hm = parse_cc_hashmap(&item);

    let expected_hm = {
        let mut hm = HashMap::new();
        hm.insert("ca".to_string(), 1);
        hm.insert("ch".to_string(), 2);
        hm
    };

    assert_eq!(hm, expected_hm);
}

#[test]
fn test_timestamp_and_interval() {
    let s = "dirreq-stats-end 2018-01-29 17:01:28 (86400 s)";
    let item = DescItem::new(s);
    let parsed = parse_timestamp_and_interval(&item);
    let expected = TimestampAndInterval {
        timestamp: datestring_to_datetime("2018-01-29 17:01:28").unwrap(),
        interval: 86400
    };
    assert_eq!(parsed, Ok(expected));
}

#[test]
fn test_parse_dl_stats() {
    let s = "dirreq-v3-direct-dl complete=1,timeout=2,running=3";
    let item = DescItem::new(s);
    let dl_stats = parse_dl_stats(&item);

    let expected = {
        let mut hm = HashMap::new();
        hm.insert(DownloadState::Complete, 1);
        hm.insert(DownloadState::Timeout, 2);
        hm.insert(DownloadState::Running, 3);
        hm
    };

    assert_eq!(dl_stats, expected);
}

#[test]
fn test_parse_history() {
    let s = "write-history 2018-01-29 16:57:00 (14400 s) 4032768000,5343528960";
    let item = DescItem::new(s);
    let parsed = parse_history(&item);

    let expected_history = History {
        timestamp: datestring_to_datetime("2018-01-29 16:57:00").unwrap(),
        interval: 14400,
        bytes_list: vec![4032768000, 5343528960]
    };

    assert!(parsed.is_ok());
    assert_eq!(parsed.unwrap(), expected_history);
}

#[test]
fn test_parse_hs_stats() {
    let s = "hidserv-rend-relayed-cells -14878 delta_f=2048 epsilon=0.30 bin_size=1024";
    let item = DescItem::new(s);
    let hs_stats = parse_hs_stats(&item);

    let expected_hs_stats = HidservStats {
        n_seen: -14878,
        delta_f: 2048,
        epsilon: 0.30,
        bin_size: 1024
    };

    assert_eq!(hs_stats, expected_hs_stats);
}

#[test]
fn test_parse_bridge_ip_transports() {
    let s = "bridge-ip-transports <OR>=8,obfs4=16";
    let item = DescItem::new(s);
    let transport_counter = parse_bridge_ip_transports(&item);

    let expected_transport_counter = {
        let mut hm = HashMap::new();
        hm.insert(PluggableTransport::Or, 8);
        hm.insert(PluggableTransport::Obfs4, 16);
        hm
    };

    assert_eq!(transport_counter, expected_transport_counter);
}

#[test]
fn test_parse_dirreq_v3_resp() {
    let s = "dirreq-v3-resp ok=1,not-enough-sigs=2";
    let item = DescItem::new(s);
    let resp_counter = parse_dirreq_v3_resp(&item);

    let expected_resp_counter = {
        let mut hm = HashMap::new();
        hm.insert(ResponseStatus::Ok, 1);
        hm.insert(ResponseStatus::NotEnoughSigs, 2);
        hm
    };

    assert_eq!(resp_counter, expected_resp_counter);
}

#[test]
fn test_parse_extra_info() {
    let s = "extra-info Unnamed ADC6C6B8767F254A5DAEC275E5D664D3E93063CA";
    let item = DescItem::new(s);
    let parsed = parse_extra_info(&item);

    let expected_extra_info = ExtraInfo {
        nick: "Unnamed".to_string(),
        fp: "ADC6C6B8767F254A5DAEC275E5D664D3E93063CA".to_string()
    };

    assert_eq!(parsed, Ok(expected_extra_info));
}

#[test]
fn test_parse_transport() {
    let s = "transport scramblesuit 127.0.0.1:9050";
    let item = DescItem::new(s);
    let parsed = parse_transport(&item);

    let expected_transport = Transport {
        transport_type: PluggableTransport::Scramblesuit,
        addr: Some("127.0.0.1:9050".to_string()),
        args: None
    };

    assert_eq!(parsed, Ok(expected_transport));
}

#[test]
fn test_parse_padding_counts() {
    let s = "padding-counts 2018-01-29 16:33:01 (86400 s) bin-size=1 write-drop=2";
    let item = DescItem::new(s);
    let padding_counts = parse_padding_counts(&item);

    let expected_padding_counts = PaddingCounts {
        timestamp: datestring_to_datetime("2018-01-29 16:33:01").unwrap(),
        interval: 86400,
        bin_size: Some(1),
        write_drop: Some(2),
        write_pad: None,
        write_total: None,
        read_drop: None,
        read_pad: None,
        read_total: None,
        enabled_read_pad: None,
        enabled_read_total: None,
        enabled_write_pad: None,
        enabled_write_total: None,
        max_chanpad_timers: None
    };

    assert_eq!(padding_counts, expected_padding_counts);
}
