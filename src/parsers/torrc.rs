use descriptor::DescItem;

pub fn parse_control_port(item: &DescItem) -> Result<u16, ()> {
    item.data().split_whitespace()
        .nth(0).ok_or(())?
        .parse().map_err(|_| ())
}
