use std::io::Read;

use descriptor::{DescMeta, Descriptor};
use iter::{ItemIter, PeekString};
use keywords::ALL_KEYWORDS;

/// Iterates over the descriptors found in a document.
#[derive(Debug)]
pub struct DescIter<R: Read> {
    items: ItemIter<R>,
    meta: DescMeta,
    on_first_iteration: bool
}

impl<R: Read> DescIter<R> {
    pub fn from_reader(readable: R) -> Self {
        let mut items = ItemIter::from_reader(readable, &ALL_KEYWORDS);

        let meta = if let Some(first_line) = items.lines.peek_string() {
            DescMeta::from_str(&first_line)
        } else {
            DescMeta::unknown()
        };

        DescIter { items, meta, on_first_iteration: true }
    }
}

impl<R: Read> Iterator for DescIter<R> {
    type Item = Descriptor;

    fn next(&mut self) -> Option<Self::Item> {
        if self.on_first_iteration {
            if let Some(ref delimiter) = self.meta.delimiter {
                self.items.skip_to_keyword(&delimiter);
            }
            self.on_first_iteration = false;
        }

        let first_item = self.items.next()?;
        if first_item.keyword() == "directory-footer" { return None; }
        let mut desc = Descriptor::empty();
        desc.update(first_item);

        while let Some(peeked_item) = self.items.peek() {
            let next_keyword = peeked_item.keyword();
            if next_keyword == "directory-footer" { break; }

            if let Some(ref delimiter) = self.meta.delimiter {
                if next_keyword == delimiter { break; }
            }

            let item = self.items.next().unwrap();
            desc.update(item);
        }

        if desc.is_empty() { None } else { Some(desc) }
    }
}

#[test]
fn test_desc_iter() {
    let bytes =
        "@type bridge-server-descriptor 1.2\n\
        master-key-ed25519 EgEFOJ2gtWuZx02wkYwt8XGRwEYMncp41J2fHikdrfc\n\
        @type bridge-server-descriptor 1.2\n\
        master-key-ed25519 mZnCifY+01kWdPQaLTjCTdNMQ+1ukkU1Z56zWV6wOrY".as_bytes();

    let mut desc_iter = DescIter::from_reader(bytes);
    let first = desc_iter.next();
    let second = desc_iter.next();
    let third = desc_iter.next();

    assert!(first.is_some());
    assert!(second.is_some());
    assert!(third.is_none());

    assert_eq!(
        first.unwrap().master_key_ed25519,
        Some("EgEFOJ2gtWuZx02wkYwt8XGRwEYMncp41J2fHikdrfc".to_string())
    );

    assert_eq!(
        second.unwrap().master_key_ed25519,
        Some("mZnCifY+01kWdPQaLTjCTdNMQ+1ukkU1Z56zWV6wOrY".to_string())
    );
}

#[test]
fn test_desc_iter_with_header_and_footer() {
    let bytes =
        "@type network-status-consensus-3 1.0\n\
        network-status-version 3\n\
        r seele AAoQ1DAR6kkoo19hBAX5K0QztNw o8Jn4OqyDILiaJUaNFDiXB/A1wA 2018-02-02 14:40:27 67.161.31.147 9001 0\n\
        s Running Stable V2Dir Valid\n\
        v Tor 0.3.1.9\n\
        r novatorrelay BZP1JVMWdIJH66djU6OmH2IiSQM gGEtc1lxAcgOqmW9dlsrvoYs2kg 2018-02-02 14:59:27 93.174.93.71 443 9030\n\
        directory-footer\n\
        whatever\n\
        whatever".as_bytes();

    let mut desc_iter = DescIter::from_reader(bytes);
    let first = desc_iter.next();
    let second = desc_iter.next();
    let third = desc_iter.next();

    assert!(first.is_some());
    assert!(second.is_some());
    assert!(third.is_none());

    assert_eq!(
        first.unwrap().r.unwrap().nick,
        "seele".to_string()
    );

    assert_eq!(
        second.unwrap().r.unwrap().nick,
        "novatorrelay".to_string()
    );
}
