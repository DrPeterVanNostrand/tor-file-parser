use std::io::{BufRead, BufReader, Lines, Read};
use std::iter::Peekable;

use descriptor::DescItem;

type PeekableLines<R> = Peekable<Lines<BufReader<R>>>;

pub trait PeekString {
    fn peek_string(&mut self) -> Option<String>;
}

impl<R> PeekString for PeekableLines<R>
    where R: Read
{
    // If the peeked line contains invalid UTF-8 bytes, we return None.
    // This causes anything iterating over a PeekableLines iterator to stop
    // prior to parsing the invalid bytes.
    fn peek_string(&mut self) -> Option<String> {
        if let Some(to_string_res) = self.peek() {
            if let Ok(ref s) = *to_string_res {
                return Some(s.to_string());
            }
        }
        None
    }
}

/// Iterates over the items (as defined by the dir-spec) for a given
/// descriptor file.
#[derive(Debug)]
pub struct ItemIter<R: Read> {
    pub lines: PeekableLines<R>,
    keywords: Vec<&'static str>,
    peeked: Option<DescItem>,
    stop_keyword: Option<String>,
    on_first_iteration: bool
}

impl<R> ItemIter<R>
    where R: Read
{
    pub fn from_reader(readable: R, keywords: &[&'static str]) -> Self {
        ItemIter {
            lines: BufReader::new(readable).lines().peekable(),
            keywords: keywords.to_vec(),
            peeked: None,
            stop_keyword: None,
            on_first_iteration: true
        }
    }

    pub fn skip_to_keyword(&mut self, keyword: &str) {
        while let Some(peeked_line) = self.lines.peek_string() {
            let first_word = peeked_line.split_whitespace().nth(0).unwrap();
            if first_word == keyword { break; }
            let _ = self.lines.next();
        }
    }

    pub fn peek(&mut self) -> Option<DescItem> {
        if self.peeked.is_none() {
            self.peeked = self.next();
        }
        self.peeked.clone()
    }
}

impl<R> Iterator for ItemIter<R>
    where R: Read
{
    type Item = DescItem;

    fn next(&mut self) -> Option<Self::Item> {
        if self.peeked.is_some() {
            let item = self.peeked.clone();
            self.peeked = None;
            return item;
        }

        let keyword_line = self.lines.next()?.ok()?.to_string();

        if let Some(ref stop_keyword) = self.stop_keyword {
            let keyword = keyword_line.split_whitespace().nth(0).unwrap();
            if keyword == stop_keyword { return None; }
        }

        let mut item_lines = vec![keyword_line];

        if self.on_first_iteration {
            self.on_first_iteration = false;
        }

        while let Some(peeked_line) = self.lines.peek_string() {
            let peeked_line = peeked_line.trim();

            if peeked_line.is_empty() {
                let _ = self.lines.next();
                continue;
            }

            let first_word = peeked_line.split_whitespace().nth(0).unwrap();
            if self.keywords.contains(&first_word) { break; }
            let line = self.lines.next().unwrap().unwrap().to_string();
            item_lines.push(line);
        }

        self.peeked = None;
        let item = DescItem::from_lines(item_lines);
        Some(item)
    }
}

#[test]
fn test_item_iter() {
    let bytes = "one two\nthree\nfour".as_bytes();
    let mut item_iter = ItemIter::from_reader(bytes, &["one", "four"]);

    assert!(item_iter.peeked.is_none());
    assert_eq!(item_iter.peek(), Some(DescItem::new("one two\nthree")));
    assert_eq!(item_iter.peeked, Some(DescItem::new("one two\nthree")));

    let first = item_iter.next();
    assert_eq!(first, Some(DescItem::new("one two\nthree")));
    assert!(item_iter.peeked.is_none());
    assert_eq!(item_iter.peek(), Some(DescItem::new("four")));

    let second = item_iter.next();
    assert_eq!(second, Some(DescItem::new("four")));
    assert!(item_iter.peeked.is_none());
    assert!(item_iter.peek().is_none());
    assert!(item_iter.next().is_none());
}
