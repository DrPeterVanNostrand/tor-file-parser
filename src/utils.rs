use std::collections::HashMap;

use base64;
use chrono::{DateTime, TimeZone, Utc};
use hex;

use descriptor::DescItem;
use keywords::{DATA_OPTIONAL_ITEMS, DELIMITER_KEYWORDS};

// Checks if data is required for a given item.
pub fn item_requires_data(item: &DescItem) -> bool {
    !DATA_OPTIONAL_ITEMS.contains(&item.keyword())
}

// Checks if an item demarcates a new descriptor.
pub fn is_delimiter_item(item: &DescItem) -> bool {
    DELIMITER_KEYWORDS.contains(&item.keyword())
}

// Checks if a string is base64 encoded.
pub fn is_base64_encoded(s: &str) -> bool {
    base64::decode(s).is_ok()
}

// Checks if a string is hex encoded.
pub fn is_hex_encoded(s: &str) -> bool {
    hex::decode(s).is_ok()
}

// Split a string containing key-value pairs into a hashmap.
pub fn split_into_key_value_pairs(s: &str, sep: char) -> HashMap<String, String> {
    s.split(sep).filter_map(|pair| {
        let split: Vec<_> = pair.split('=').collect();
        if split.len() != 2 { return None; }
        let key = split[0].to_string();
        let value = split[1].to_string();
        Some((key, value))
    }).collect()
}

// Convert a string formated as a descriptor "timestamp" to a UTC datetime.
pub fn datestring_to_datetime(datestring: &str) -> Result<DateTime<Utc>, ()> {
    Utc.datetime_from_str(&datestring, "%Y-%m-%d %H:%M:%S")
        .map_err(|_| ())
}

// Converts a base64 encoded string to a hex encoded string.
pub fn base64_to_hex(s: &str) -> Result<String, ()> {
    let bytes = base64::decode(s).map_err(|_| ())?;
    let hex_string = hex::encode(&bytes);
    Ok(hex_string)
}
