# tor-file-parser

A file parser (implemented in Rust) for Tor descriptors.

# About

The following
(taken from [metrics.torproject.org](https://metrics.torproject.org/collector.html))
provides a brief overview of what Tor descriptor files are:

    "Relays and directory authorities publish relay descriptors, so that
    clients can select relays for their paths through the Tor network.
    All these relay descriptors are specified in the Tor directory protocol".


This crate parses server, network-status, and extra-info descriptors.
If a file contains more than one descriptor, this crate can parse out each
descriptor individually. Each of the parser functions in this crate follows
the format specified in the
[Tor dir-spec](https://gitweb.torproject.org/torspec.git/tree/dir-spec.txt).

A large collection of recently published descriptor files can be found at
[CollecTor](https://collector.torproject.org/recent/).

Much of this project has been a port of other descriptor parsers from
Python to Rust, in particular, the following two libraries:

[Stem's `descriptor` module](https://gitweb.torproject.org/stem.git/tree/stem/descriptor)

[BridgeDB's `parse` module](https://gitweb.torproject.org/bridgedb.git/tree/bridgedb/parse)

# Installation

    $ git clone https://gitlab.com/DrPeterVanNostrand/tor-file-parser.git

You can run `cargo test` to ensure that everything is working properly.

    $ cd tor-file-parser
    $ cargo test

To use this crate from a project of your own, add the following to your
project's `Cargo.toml`:

    [dependencies]
    tor-file-parser = { path = "<location where you cloned the repo>" }

if you haven't cloned the repo, you can use the git url instead:

    [dependencies]
    tor-file-parser = { git = "https://gitlab.com/DrPeterVanNostrand/tor-file-parser" }

# Usage

### Parsing a File Containing a Single Descriptor

```rust
extern crate tor_file_parser;

use std::io::File;

use tor_file_parser::Descriptor;

fn main() {
    let file = File::open("/path/to/descriptor").unwrap();
    let desc = Descriptor::new(&file);
    println!("{:#?}", desc);
}
```

### Parsing a File Containing Mutliple Descriptors

```rust
extern crate tor_file_parser;

use std::io::File;

use tor_file_parser::{DescIter, Footer, Header};

fn main() {
    let file = File::open("~/.tor/cached-microdesc-consensus").unwrap();

    let header = Header::from_reader(&file);
    let footer = Footer::from_reader(&file);

    for desc in DescIter::from_reader(&file) {
        println!("{:?}", desc.r);
    }
}
```

# Contributing

Details on how to contribute can be found in `CONTRIBUTING.md`.

# TODO

This crate has not yet implemented a parser function for every item
enumerated in Tor's
[dir-spec](https://gitweb.torproject.org/torspec.git/tree/dir-spec.txt).
If you would like to contribute, you are welcome to write a parser for
an item that you do not see in `src/keywords.rs` and submit a pull request.

Better error handling for the parser functions (eg. what should the parser
do when it tries to parse "asdfasdf" to a f32). Should the error be logged,
item be silently skipped, or as this would indicate a malformed descriptor,
should parsing be stopped altogether?
